package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.db.TextInput;

import java.io.Reader;
import java.nio.file.Path;
import java.util.function.Function;
import java.util.stream.Stream;

public final class JDux {

    private static final JDuxContext GlobalContext = newContext();

    private JDux() {}

    public static <E> Function<JsonNode, E> asA(Class<E> type) {
        return node -> node.asA(type);
    }

    /**
     * Use reflection to create a JSON node from any supported object.
     */
    public static JsonNode node(Object object) {
        return GlobalContext.node(object);
    }

    /**
     * Write a node to given output using the default writer.
     */
    public static void write(JsonNode node, Appendable out) {
        GlobalContext.write(node, out);
    }

    /**
     * Write an object as JSON to given output using the default writer.
     */
    public static void write(Object obj, Appendable out) {
        GlobalContext.write(obj, out);
    }

    public static String writeToString(Object obj) {
        return GlobalContext.writeToString(obj);
    }

    /**
     * Write a stream of nodes to a string as though they are an array node.
     */
    public static String toString(Stream<JsonNode> nodes) {
        return GlobalContext.toString(nodes);
    }

    /**
     * Parse with default parser.
     */
    public static JsonNode parse(String str) {
        return GlobalContext.parse(str);
    }

    /**
     * Parse with default parser.
     */
    public static JsonNode parse(Reader reader) {
        return GlobalContext.parse(reader);
    }

    /**
     * Parse with default parser.
     */
    public static JsonNode parse(TextInput text) {
        return GlobalContext.parse(text);
    }

    /**
     * Create in-memory JSON database.
     * @param size number of bytes to allocate
     */
    public static JsonDB memDB(int size) {
        return GlobalContext.memDB(size);
    }

    /**
     * Create a file JSON database.
     * @param path path to your chosen JSON file
     */
    public static JsonDB fileDB(Path path) {
        return GlobalContext.fileDB(path);
    }

    /**
     * Create a JSON database from a folder of JSON files where the root object has fields equal to the
     * file names (without extensions) and values equal to the contents of the file.
     *
     * This can be useful when you want to treat the different root paths as tables or separate databases.
     * @param path path to the directory containing JSON files
     */
    public static JsonDB folderDB(Path path) {
        return GlobalContext.folderDB(path);
    }

    public static void setPretty() {
        GlobalContext.setPretty();
    }

    public static JDuxContext newContext() {
        return new JDuxContextImpl();
    }

}
