package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.db.LabelledNodeDecorator;
import com.gitlab.bhamilton.jdux.reflect.JsonReflectException;
import com.gitlab.bhamilton.jdux.reflect.NoSuitableConstructorException;
import com.gitlab.bhamilton.jdux.util.EntryPairs;
import com.gitlab.bhamilton.jdux.util.Subject;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Arrays.stream;
import static java.util.Comparator.comparingInt;
import static com.gitlab.bhamilton.jdux.util.Streams.merge;
import static com.gitlab.bhamilton.jdux.reflect.NodeReflection.ChildNodeMap;
import static java.util.stream.Collectors.toMap;

public interface ObjectNode extends JsonNode, EntryPairs<String, JsonNode> {

    default JsonNodeType type() {
        return JsonNodeType.object;
    }

    /**
     * Merge two object nodes.
     */
    default ObjectNode append(JsonNode node) {
        if (node instanceof ObjectNode other)
            return () -> Stream.concat(ObjectNode.this.children(), other.children());
        else if (node instanceof LabelledNode ln)
            return () -> Stream.concat(ObjectNode.this.children(), Stream.of(ln));
        throw new IllegalArgumentException("Object nodes can be extended with other object nodes or labelled nodes. " +
            "Provided: " + node.jsonString());
    }

    default ObjectNode subset(Predicate<LabelledNode> children) {
        return () -> ObjectNode.this.children().filter(children);
    }

    @SuppressWarnings("unchecked")
    @Override
    default <E> E asA(Class<E> type) {
        if (!type.isRecord())
            throw new NonRecordTypeException(type);
        ChildNodeMap childNodeMap = new ChildNodeMap(children());
        return (E) stream(type.getConstructors())
            .max(comparingInt(childNodeMap::numberOfMatchingFields))
            .map(childNodeMap::callConstructor)
            .orElseThrow(() -> new NoSuitableConstructorException(type));
    }

    @Override
    default Object asA(Type type) {
        if (type instanceof Class<?> c)
            return asA(c);
        if (type instanceof ParameterizedType pt) {
            if (pt.getRawType().equals(Optional.class))
                return Optional.of(asA(pt.getActualTypeArguments()[0]));
            else if (pt.getRawType().equals(Subject.class))
                return Subject.of(asA(pt.getActualTypeArguments()[0]));
            else if (pt.getRawType().equals(Map.class) && pt.getActualTypeArguments()[0] == String.class)
                return children().collect(toMap(LabelledNode::label, child -> child.asA(pt.getActualTypeArguments()[1])));
        }
        throw new JsonReflectException("Type " + type + " not supported for object node conversion");
    }

    @Override
    default Iterator<? extends LabelledNode> childIterator() {
        return children().iterator();
    }

    @Override
    default <E> Stream<E> entries(BiFunction<String, JsonNode, E> mappingFunction) {
        return children().map(labelledNode -> mappingFunction.apply(labelledNode.label(), labelledNode.unlabelled()));
    }

    @Override
    default void entries(BiConsumer<String, JsonNode> biConsumer) {
        children().forEach(labelledNode -> biConsumer.accept(labelledNode.label(), labelledNode.unlabelled()));
    }

    @Override
    Stream<? extends LabelledNode> children();

    /**
     * Combine the given object into this one, overriding common properties.
     */
    default ObjectNode put(ObjectNode other) {
        return () -> merge(children(), other.children(), LabelledNode::label);
    }

    default ObjectNode put(String label, JsonNode node) {
        var labelledNode = new LabelledNodeDecorator(label, node);
        return () -> merge(children(), Stream.of(labelledNode), LabelledNode::label);
    }

    @Override
    default boolean isLeaf() {
        return false;
    }

    default JsonNode get(String key) {
        return children()
            .filter(n -> n.label().equals(key))
            .map(LabelledNode::unlabelled)
            .findFirst().orElseGet(NullNode::new);
    }

    class NonRecordTypeException extends JsonReflectException {
        public NonRecordTypeException(Class<?> type) {
            super("Expected record type but was " + type.getSimpleName());
        }
    }

}
