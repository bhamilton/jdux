package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.db.TextInput;

public class JsonParseException extends IllegalArgumentException {

    protected TextInput text;

    public JsonParseException(String msg) {
        super(msg);
    }
    public JsonParseException(String msg, TextInput text) {
        super(msg);
        this.text = text;
    }
    public JsonParseException(Throwable cause, TextInput text) {
        super(cause);
        this.text = text;
    }
    public JsonParseException setText(TextInput text) {
        this.text = text;
        return this;
    }
    @Override
    public String getMessage() {
        return (super.getMessage() == null ? "" : super.getMessage())
            + (text == null ? "" : " at index "+text.index()+", \"..." + text.readLine() + "\"");
    }

}
