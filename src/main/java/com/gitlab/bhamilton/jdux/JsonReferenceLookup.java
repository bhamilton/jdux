package com.gitlab.bhamilton.jdux;

public interface JsonReferenceLookup {

    JsonReferenceLookup EMPTY = JsonNode.StringNode::new;

    JsonNode lookup(String reference);

}
