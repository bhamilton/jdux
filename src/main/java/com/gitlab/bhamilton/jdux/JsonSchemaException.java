package com.gitlab.bhamilton.jdux;

public class JsonSchemaException extends IllegalArgumentException {

    public JsonSchemaException(String s) {
        super(s);
    }

}
