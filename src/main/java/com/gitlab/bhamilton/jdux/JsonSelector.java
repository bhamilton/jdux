package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.util.Criteria;
import com.gitlab.bhamilton.jdux.util.Streams;

import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;

import static java.util.stream.Collectors.joining;

/**
 * A LinkedList-style selector for JSON nodes.  Each segment needs to be processed separately for better tree-shaking
 * during querying and updating.
 */
public interface JsonSelector extends Iterable<JsonSelectorSegment>, Criteria<JsonNode> {

    JsonSelector IDENTITY = new JsonSelector() {
        @Override
        public boolean intersects(Criteria<JsonNode> other) {
            return true;
        }
        @Override
        public boolean test(JsonNode node) {
            return true;
        }
        @Override
        public JsonSelectorSegment head() {
            return JsonSelectorSegment.IDENTITY;
        }
        @Override
        public JsonSelector next() {
            return JsonSelector.IDENTITY;
        }
        @Override
        public boolean contains(Criteria<JsonNode> other) {
            return other == IDENTITY;
        }
        @Override
        public Iterator<JsonSelectorSegment> iterator() {
            return Collections.singletonList(JsonSelectorSegment.IDENTITY).iterator();
        }
        @Override
        public JsonSelector previous() {
            return JsonSelector.IDENTITY;
        }
        @Override
        public String toString() {
            return "";
        }
    };

    default int length() {
        var count = 0;
        var iter = iterator();
        for (; iter.hasNext(); iter.next())
            count++;
        return count;
    }

    JsonSelectorSegment head();

    default JsonSelectorSegment tail() {
        var tail = head();
        for (var segment : this)
            tail = segment;
        return tail;
    }

    /**
     * Get the selector with the last element removed.
     */
    JsonSelector previous();

    JsonSelector next();

    default boolean hasNext() {
        return next() != IDENTITY;
    }

    @Override
    default boolean intersects(Criteria<JsonNode> other) {
        if (other instanceof JsonSelector s)
            return head().intersects(s.head());
        return true;
    }

    @Override
    default boolean test(JsonNode node) {
        return head().test(node);
    }

    default Optional<String> key() {
        return head().key();
    }

    default JsonDepth depth() {
        return head().depth();
    }

    default String stringValue() {
        return Streams.toStream(this)
            .map(JsonSelectorSegment::stringValue)
            .collect(joining());
    }
}
