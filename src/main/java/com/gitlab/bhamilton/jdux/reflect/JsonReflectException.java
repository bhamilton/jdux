package com.gitlab.bhamilton.jdux.reflect;

public class JsonReflectException extends RuntimeException {
    public JsonReflectException(Throwable cause) {
        super(cause);
    }
    public JsonReflectException(String message) {
        super(message);
    }
}
