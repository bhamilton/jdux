package com.gitlab.bhamilton.jdux.reflect;

import com.gitlab.bhamilton.jdux.ArrayNode;
import com.gitlab.bhamilton.jdux.JsonNode;
import com.gitlab.bhamilton.jdux.ObjectNode;
import com.gitlab.bhamilton.jdux.util.Subject;
import com.gitlab.bhamilton.jdux.db.LabelledNodeDecorator;

import java.lang.reflect.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.stream.Stream;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class NodeReflection {

    public JsonNode toNode(Object obj) {
        if (obj == null)
            return new JsonNode.NullNode();
        if (obj instanceof String s)
            return new JsonNode.StringNode(s);
        if (obj instanceof TemporalAccessor ta)
            return new JsonNode.StringNode(DateTimeFormatter.ISO_INSTANT.format(ta));
        if (obj instanceof Collection<?> collection)
            return new ListNode(collection.stream()
                .map(this::toNode)
                .collect(toList()));
        if (obj instanceof Map<?, ?> map)
            return new MapNode(map);
        if (obj instanceof Subject<?> s)
            return toNode(s.get());
        if (obj instanceof Optional<?> o)
            return o.isPresent() ? toNode(o.get()) : new JsonNode.NullNode();
        Class<?> type = obj.getClass();
        if (type.isPrimitive() || Primitives.isWrapperType(type))
            return type.equals(Boolean.class) || type.equals(Boolean.TYPE)
                ? new JsonNode.BooleanNode((Boolean) obj)
                : new JsonNode.NumberNode((Number) obj);
        if (type.isEnum())
            return new JsonNode.StringNode(obj.toString());
        if (type.isArray())
            return new ListNode(Arrays.stream((Object[]) obj)
                .map(this::toNode)
                .collect(toList()));
        if (type.isRecord())
            return new RecordNode(obj);
        throw new JsonReflectException("Unsupported type " + obj);
    }

    public static class ListNode implements ArrayNode {

        private final List<JsonNode> nodes;

        public ListNode(List<JsonNode> nodes) {
            this.nodes = nodes;
        }

        @Override
        public Iterator<? extends JsonNode> childIterator() {
            return nodes.iterator();
        }

        @Override
        public Stream<? extends JsonNode> children() {
            return nodes.stream();
        }

        @Override
        public String toString() {
            return jsonString();
        }
    }

    private class RecordNode implements ObjectNode {

        private final Object record;

        public RecordNode(Object record) {
            this.record = record;
        }

        @Override
        public Stream<? extends LabelledNode> children() {
            return Stream.of(record.getClass().getDeclaredFields())
                .filter(f -> !Modifier.isStatic(f.getModifiers()))
                .map(this::asLabeledNode);
        }

        private LabelledNode asLabeledNode(Field field) {
            try {
                Method getter = record.getClass().getMethod(field.getName());
                JsonNode value = toNode(getter.invoke(record));
                return new LabelledNodeDecorator(field.getName(), value);
            } catch (ReflectiveOperationException e) {
                throw new JsonReflectException(e);
            }
        }

        @Override
        public String toString() {
            return jsonString();
        }

    }

    public class MapNode implements ObjectNode {

        private final Map<?, ?> map;

        public MapNode(Map<?, ?> map) {
            this.map = map;
        }

        @Override
        public Stream<? extends LabelledNode> children() {
            return map.entrySet().stream()
                .map(e -> new LabelledNodeDecorator(String.valueOf(e.getKey()), toNode(e.getValue())));
        }

        @Override
        public String toString() {
            return jsonString();
        }
    }

    /**
     * Simple wrapper for performing reflection on object node's children.
     */
    public static record ChildNodeMap(Map<String, JsonNode>childMap) {

        public ChildNodeMap(Stream<? extends JsonNode.LabelledNode> children) {
            this(children.collect(toMap(JsonNode.LabelledNode::label, JsonNode.LabelledNode::unlabelled)));
        }

        public int numberOfMatchingFields(Constructor<?> ctor) {
            return (int) stream(ctor.getParameters())
                .map(Parameter::getName)
                .filter(childMap::containsKey)
                .count();
        }

        public Object callConstructor(Constructor<?> ctor) {
            try {
                Object[] args = stream(ctor.getParameters())
                    .map(param -> childMap.getOrDefault(param.getName(), new JsonNode.NullNode()).asA(param.getParameterizedType()))
                    .toArray();
                return ctor.newInstance(args);
            } catch (ReflectiveOperationException | RuntimeException e) {
                throw new JsonReflectException(e);
            }
        }
    }
}
