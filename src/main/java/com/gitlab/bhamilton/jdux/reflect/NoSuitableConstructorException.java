package com.gitlab.bhamilton.jdux.reflect;

public class NoSuitableConstructorException extends JsonReflectException {
    public NoSuitableConstructorException(Class<?> type) {
        super("No suitable constructor found for type \"" + type.getSimpleName() + "\"");
    }
}
