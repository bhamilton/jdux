package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.db.FolderCacheMap;
import com.gitlab.bhamilton.jdux.db.IORuntimeException;
import com.gitlab.bhamilton.jdux.db.JsonParser;
import com.gitlab.bhamilton.jdux.db.JsonWriter;
import com.gitlab.bhamilton.jdux.db.RoutingJsonDB;
import com.gitlab.bhamilton.jdux.db.StreamingJsonDB;
import com.gitlab.bhamilton.jdux.db.TextInput;
import com.gitlab.bhamilton.jdux.reflect.NodeReflection;
import com.gitlab.bhamilton.jdux.util.CacheMap;

import java.io.Flushable;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.CharBuffer;
import java.nio.channels.Channels;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashSet;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;

record JDuxContextImpl(JsonWriter writer, JsonParser parser, NodeReflection reflection) implements JDuxContext {

    JDuxContextImpl() {
        this(new JsonWriter(), new JsonParser(), new NodeReflection());
    }

    /**
     * Use reflection to create a JSON node from any supported object.
     */
    @Override
    public JsonNode node(Object object) {
        return reflection.toNode(object);
    }

    /**
     * Write a node to given output using the default writer.
     */
    @Override
    public void write(JsonNode node, Appendable out) {
        writer.write(node, out);
    }

    /**
     * Write a stream of nodes to a string as though they are an array node.
     */
    @Override
    public void write(Stream<JsonNode> nodes, Appendable out) {
        writer.write(nodes, out);
    }

    /**
     * Write an object as JSON to given output using the default writer.
     */
    @Override
    public void write(Object obj, Appendable out) {
        writer.write(node(obj), out);
    }

    /**
     * Parse with default parser.
     */
    @Override
    public JsonNode parse(String str) {
        return parser.parse(TextInput.wrap(str));
    }

    /**
     * Parse with default parser.
     */
    @Override
    public JsonNode parse(Reader reader) {
        return parser.parse(TextInput.wrap(reader));
    }

    @Override
    public JsonNode parse(TextInput text) {
        return parser.parse(text);
    }

    /**
     * Create in-memory JSON database.
     * @param size number of bytes to allocate
     */
    @Override
    public JsonDB memDB(int size) {
        return new StreamingJsonDB(new ReadWriteBuffers(size), parser, writer);
    }

    /**
     * Create a file JSON database.
     * @param path path to your chosen JSON file
     */
    @Override
    public JsonDB fileDB(Path path) {
        return fileDB(null, path);
    }

    private JsonDB fileDB(JsonDB parent, Path path) {
        createEmptyFileIfDoesNotExist(path);
        return new StreamingJsonDB(new FileChannels(path), parser.withLookup(parent), writer);
    }

    public void createEmptyFileIfDoesNotExist(Path path) {
        try {
            if (!Files.exists(path))
                Files.createFile(path);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * Create a JSON database from a folder of JSON files where the root object has fields equal to the
     * file names (without extensions) and values equal to the contents of the file.
     *
     * This can be useful when you want to treat the different root paths as tables or separate databases.
     * @param path path to the directory containing JSON files
     */
    @Override
    public JsonDB folderDB(Path path) {
        try {

            if (!Files.exists(path))
                Files.createDirectories(path);

            var fileDBFactory = new FileDBFactory();
            var fileDatabases = new FolderCacheMap<>(
                path,
                key -> key + ".json",
                fileName -> fileName.substring(0, fileName.length() - 5),
                fileDBFactory,
                (file, jsonDB) -> fileDB(file).root(jsonDB.root())
            );
            var routingDB = new RoutingJsonDB(CacheMap.hashCache(fileDatabases));
            fileDBFactory.parent = routingDB;

            return routingDB;

        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public String toString() {
        return null;
    }

    /**
     * Set the writer output to pretty.
     */
    @Override
    public void setPretty() {
        writer.setPretty(true);
    }

    private static class FileChannels implements StreamingJsonDB.StreamOptions<Writer> {

        private final SeekableByteChannel read, write;

        public FileChannels(Path file) {
            try {
                this.read = Files.newByteChannel(file, new HashSet<>(Collections.singleton(READ)));
                this.write = Files.newByteChannel(file, new HashSet<>(Collections.singleton(WRITE)));
            } catch (IOException e) {
                throw new IORuntimeException(e);
            }
        }

        @Override
        public TextInput input() {
            try {
                return TextInput.wrap(Channels.newReader(read.position(0), StandardCharsets.US_ASCII));
            } catch (IOException e) {
                throw new IORuntimeException(e);
            }
        }

        @Override
        public Writer output() {
            try {
                return Channels.newWriter(write.position(0), StandardCharsets.US_ASCII);
            } catch (IOException e) {
                throw new IORuntimeException(e);
            }
        }

        @Override
        public void after(Appendable output) {
            try {
                if (output instanceof Flushable f)
                    f.flush();
                // when the json shrinks, we need to truncate
                write.truncate(write.position());
            } catch (IOException e) {
                throw new IORuntimeException(e);
            }
        }
    }
    private static class ReadWriteBuffers implements StreamingJsonDB.StreamOptions<CharBuffer> {

        private final CharBuffer read, write;

        ReadWriteBuffers(int size) {
            this.write = CharBuffer.allocate(size);
            this.read = write.asReadOnlyBuffer();
        }

        @Override
        public TextInput input() {
            read.position(0);
            return TextInput.wrap(read.asReadOnlyBuffer());
        }

        @Override
        public CharBuffer output() {
            write.position(0);
            return write;
        }

        @Override
        public void after(Appendable output) {} // do nothing
    }

    /**
     * Stateful factory for injecting parent.
     */
    private class FileDBFactory implements Function<Path, JsonDB> {

        private JsonDB parent;

        @Override
        public JsonDB apply(Path path) {
            return fileDB(parent, path);
        }

    }

}
