package com.gitlab.bhamilton.jdux;

import java.util.Map;

// TODO required
public record JsonSchema(
    JsonNodeType type,
    Map<String, JsonSchema> properties,     // types of properties for object node
    JsonSchema items,                       // type of child element for arrays
    String path                             // path in schema for reference
) implements JsonSchemaValidator {

    public static JsonSchema empty() {
        return new JsonSchema(null, null, null, null);
    }

    // Recursively validates schema of chain of nodes from root to leaf.
    @Override
    public void validate(JsonNodeChain chain) {
        if (type == null)
            return;
        if (!chain.type().equals(type))
            throw new JsonSchemaException(String.format("Type mismatch, expected %s but was %s in chain %s.", type, chain.type(), chain));
        switch (type) {
            case array -> validateArray(chain);
            case object -> validateObject(chain);
            default -> validateNoChild(chain);
        }
    }

    private void validateArray(JsonNodeChain chain) {
        // element schema specified via "items" property
        if (items != null) {
            // validate the full array node when not specified in chain
            if (chain.next() != null)
                items.validate(chain.next());
            else
                chain.node().children().forEach(items::validate);
        }
    }

    private void validateObject(JsonNodeChain chain) {
        // no properties, no validation needed.
        if (properties == null)
            return;
        // validate all properties if last node in chain
        else if (chain.next() == null)
            chain.node().children().forEach(child -> validateAsProperty(chain.next(child)));
        else
            validateAsProperty(chain.next());
    }

    private void validateAsProperty(JsonNodeChain chain) {
        String label = chain.label();
        if (label == null || !properties.containsKey(label))
            throw new JsonSchemaException(String.format("Expected one of %s as property but was %s", properties.keySet(), label));
        properties.get(label).validate(chain);
    }

    private void validateNoChild(JsonNodeChain chain) {
        if (chain.next() != null)
            throw new JsonSchemaException(String.format("Unexpected child for %s node in chain %s", type, chain));
    }
}
