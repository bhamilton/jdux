package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.reflect.JsonReflectException;

import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.stream.Collector;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableList;
import static java.util.stream.Collectors.toUnmodifiableSet;

public interface ArrayNode extends JsonNode {

    default JsonNodeType type() {
        return JsonNodeType.array;
    }

    default ArrayNode append(JsonNode child) {
        if (child instanceof ArrayNode other)
            return () -> Stream.concat(ArrayNode.this.children(), other.children());
        return () -> Stream.concat(ArrayNode.this.children(), Stream.of(child));
    }

    @SuppressWarnings("unchecked")
    @Override
    default <E> E asA(Class<E> type) {
        if (type.isArray()) {
            return (E) children()
                .map(n -> n.asA(type.getComponentType()))
                .toArray(len -> (Object[]) Array.newInstance(type.getComponentType(), len));
        }
        throw new JsonReflectException("Expected array or collection for array node conversion but was " + type.getName());
    }

    @Override
    default Object asA(Type type) {
        if (type instanceof Class<?> c)
            return asA(c);
        if (type instanceof ParameterizedType pt && pt.getRawType() instanceof Class<?> c && Collection.class.isAssignableFrom(c)) {
            Type typeBound = pt.getActualTypeArguments()[0];
            Collector<Object, ?, ? extends Collection<Object>> collector = switch (pt.getRawType().getTypeName()) {
                case "java.util.List", "java.util.Collection" -> toUnmodifiableList();
                case "java.util.Set" -> toUnmodifiableSet();
                default -> throw new JsonReflectException("Unrecognized collection type " + pt.getTypeName());
            };
            return children()
                .map(n -> n.asA(typeBound))
                .collect(collector);
        }
        throw new JsonReflectException("Expected array or collection for ArrayNode conversion but was " + type.getTypeName());
    }

    @Override
    default boolean isLeaf() {
        return false;
    }

}
