package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.db.LabelledNodeDecorator;
import com.gitlab.bhamilton.jdux.reflect.JsonReflectException;
import com.gitlab.bhamilton.jdux.util.Subject;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.Iterator;
import java.util.Optional;
import java.util.stream.Stream;

public interface JsonNode {

    JsonNodeType type();

    default String jsonString() {
        StringBuilder out = new StringBuilder();
        JDux.write(this, out);
        return out.toString();
    }

    /**
     * Convert to the given type.
     * @param type the desired type; must be a record if object node,
     *             collection / array if array node, or primitive / string if value node
     * @return the converted value
     */
    <E> E asA(Class<E> type);

    /**
     * Looser form of asA(Class) for generic type support.
     */
    Object asA(Type type);

    /**
     * Iterator over this node's children.
     * @return the node's children; empty if non-object / array node
     */
    default Iterator<? extends JsonNode> childIterator() {
        return children().iterator();
    }

    /**
     * Stream over this node's children.
     * @return the node's children; empty if non-object / array node
     */
    Stream<? extends JsonNode> children();

    default Stream<? extends JsonNode> descendents() {
        return children()
            .flatMap(child -> Stream.concat(Stream.of(child), child.descendents()));
    }

    boolean isLeaf();

    default Comparable value() {
        return null;
    }

    /**
     * Convenience method to perform "asA()" operation over this node's children.
     */
    default <E> Stream<E> children(Class<E> recordType) {
        return children().map(n -> n.asA(recordType));
    }

    interface LabelledNode extends JsonNode {
        String label();
        JsonNode unlabelled();

        static LabelledNode labelled(String label, JsonNode base) {
            return new LabelledNodeDecorator(label, base);
        }
    }

    @SuppressWarnings("unchecked")
    abstract class ValueNode<T> implements JsonNode {
        T value;
        ValueNode(T value) {
            this.value = value;
        }
        @Override
        public JsonNodeType type() {
            return JsonNodeType.number; // TODO
        }
        @Override
        public <E> E asA(Class<E> recordType) {
            if (recordType.isInstance(value))
                return recordType.cast(value);
            return (E) value; // TODO
        }
        @Override
        public Object asA(Type type) {
            if (type instanceof Class<?> c)
                return asA(c);
            if (type instanceof ParameterizedType pt) { // TODO duplication
                if (pt.getRawType().equals(Optional.class))
                    return Optional.of(asA(pt.getActualTypeArguments()[0]));
                else if (pt.getRawType().equals(Subject.class))
                    return Subject.of(asA(pt.getActualTypeArguments()[0]));
            }
            return value; // TODO
        }

        @Override
        public Comparable value() {
            return (Comparable) value;
        }

        @Override
        public boolean isLeaf() {
            return true;
        }

        @Override
        public Stream<? extends JsonNode> children() {
            return Stream.empty();
        }

        @Override
        public String toString() {
            return String.valueOf(value);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof ValueNode vn)
                return value.equals(vn.value);
            return false;
        }
    }

    class NullNode implements JsonNode {
        @Override
        public JsonNodeType type() {
            return JsonNodeType.nil;
        }
        @Override
        public boolean isLeaf() {
            return true;
        }
        @Override
        public <E> E asA(Class<E> recordType) {
            return null;
        }
        @Override
        public Object asA(Type type) {
            return null;
        }
        @Override
        public Stream<? extends JsonNode> children() {
            return Stream.empty();
        }
        @Override
        public String toString() {
            return "null";
        }
    }

    class BooleanNode extends ValueNode<Boolean> {
        public BooleanNode(Boolean value) {
            super(value);
        }
        @Override
        public JsonNodeType type() {
            return JsonNodeType.bool;
        }
    }

    class NumberNode extends ValueNode<Number> {
        public NumberNode(Number value) {
            super(value);
        }
        @Override
        public JsonNodeType type() {
            return JsonNodeType.number;
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    class StringNode extends ValueNode<String> {
        public StringNode(String value) {
            super(value);
        }

        @Override
        public JsonNodeType type() {
            return JsonNodeType.string;
        }

        @Override
        public <E> E asA(Class<E> type) {
            if (TemporalAccessor.class.isAssignableFrom(type))
                return (E) asTime((Class) type);
            if (type.isEnum())
                return (E) Enum.valueOf((Class) type, value);
            return super.asA(type);
        }
        @Override
        public String value() {
            return value;
        }

        // TODO UTF
        @Override
        public String toString() {
            try {
                StringBuilder sb = new StringBuilder(value.length() + 2).append('"');
                StringReader reader = new StringReader(value);
                int c;
                while ((c = reader.read()) != -1) {
                    switch (c) {
                        case '"', '\\', '/' -> sb.append('\\').append((char) c);
                        case '\f' -> sb.append("\\f");
                        case '\n' -> sb.append("\\n");
                        case '\r' -> sb.append("\\r");
                        case '\t' -> sb.append("\\t");
                        default -> sb.append((char) c);
                    }
                }
                return sb.append('"').toString();
            } catch (IOException e) {
                return '"' + value + '"';
            }
        }
        public <T extends TemporalAccessor> T asTime(Class<T> type) {
            try {
                ZonedDateTime ta = Instant.parse(value).atZone(ZoneId.systemDefault());
                return (T) type.getMethod("from", TemporalAccessor.class).invoke(null, ta);
            } catch (ReflectiveOperationException e) {
                throw new JsonReflectException(e);
            }
        }
    }

    class ReferenceNode extends StringNode {

        private final JsonReferenceLookup lookup;

        public ReferenceNode(String value, JsonReferenceLookup lookup) {
            super(value);
            this.lookup = lookup;
        }

        @Override
        public JsonNodeType type() {
            return JsonNodeType.ref;
        }

        // lookup removes hash
        public JsonNode getReference() {
            return lookup.lookup(value.substring(1));
        }

    }

}
