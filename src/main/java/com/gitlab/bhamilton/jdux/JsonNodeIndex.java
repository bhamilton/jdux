package com.gitlab.bhamilton.jdux;

public interface JsonNodeIndex {

    JsonNode resolve(String reference);

}
