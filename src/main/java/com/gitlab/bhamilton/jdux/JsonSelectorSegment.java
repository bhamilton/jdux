package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.util.Criteria;

import java.util.Optional;

public interface JsonSelectorSegment extends Criteria<JsonNode> {

    JsonSelectorSegment IDENTITY = new JsonSelectorSegment() {
        @Override
        public JsonDepth depth() {
            return JsonDepth.IDENTITY;
        }
        @Override
        public Optional<String> key() {
            return Optional.empty();
        }
        @Override
        public String stringValue() {
            return "";
        }
        @Override
        public boolean intersects(Criteria<JsonNode> other) {
            return this == other;
        }
        @Override
        public boolean contains(Criteria<JsonNode> other) {
            return this == other;
        }
        @Override
        public boolean test(JsonNode jsonNode) {
            return true;
        }
    };

    JsonDepth depth();

    Optional<String> key();

    String stringValue();

}
