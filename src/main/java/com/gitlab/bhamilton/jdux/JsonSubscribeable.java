package com.gitlab.bhamilton.jdux;

import java.util.function.Consumer;

public interface JsonSubscribeable {

    /**
     * When nodes for the given path are modified, the consumer will be called.
     * @param path     in form of a.b..c where {a,b,c} are field names in the JSON tree
     * @param consumer accepts updated nodes
     */
    void subscribe(String path, Consumer<JsonNode> consumer);

}
