package com.gitlab.bhamilton.jdux;

import java.util.function.UnaryOperator;

public interface JsonNodeUpdater extends JsonSubscribeable {

    void applySchema(JsonSchema newSchema);

    JsonNode applyUpdate(String query, JsonNode node, UnaryOperator<JsonNode> update);

}
