package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.db.JsonParser;
import com.gitlab.bhamilton.jdux.db.JsonWriter;
import com.gitlab.bhamilton.jdux.db.TextInput;
import com.gitlab.bhamilton.jdux.reflect.NodeReflection;

import java.io.Reader;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface JDuxContext {

    JsonNode node(Object object);

    void write(JsonNode node, Appendable out);

    /**
     * Write a stream of nodes to a string as though they are an array node.
     */
    void write(Stream<JsonNode> nodes, Appendable out);

    void write(Object obj, Appendable out);

    default String writeToString(Object obj) {
        StringBuilder sb = new StringBuilder();
        write(obj, sb);
        return sb.toString();
    }

    /**
     * Write a stream of nodes to a string as though they are an array node.
     */
    default String toString(Stream<JsonNode> nodes) {
        StringBuilder sb = new StringBuilder();
        write(nodes, sb);
        return sb.toString();
    }

    JsonNode parse(String str);

    JsonNode parse(Reader reader);

    JsonNode parse(TextInput text);

    /**
     * Create in-memory JSON database.
     * @param size number of bytes to allocate
     */
    JsonDB memDB(int size);

    /**
     * Create a file JSON database.  The file must be valid JSON.
     * @param path path to your chosen JSON file
     */
    JsonDB fileDB(Path path);

    /**
     * Create a JSON database from a folder of JSON files where the root object has fields equal to the
     * file names (without extensions) and values equal to the contents of the file.
     *
     * This can be useful when you want to treat the different root paths as tables or separate databases.
     * @param path path to the directory containing JSON files
     */
    JsonDB folderDB(Path path);

    void setPretty();

    JsonParser parser();

    JsonWriter writer();

    NodeReflection reflection();

}
