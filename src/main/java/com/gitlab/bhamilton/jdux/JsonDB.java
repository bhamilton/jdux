package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.db.JsonPath;

import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

public interface JsonDB extends JsonSubscribeable, JsonReferenceLookup {

    /**
     * Create a new node for the given path.
     * The path can point to an array node, or a field on an object node.
     * @param path in form of a.b..c where {a,b,c} are field names in the JSON tree
     * @param node any JSON node
     */
    default void insert(String path, JsonNode node) {
        update(path, parent -> {
            if (parent instanceof ObjectNode on)
                return on.append(node);
            else if (parent instanceof ArrayNode an)
                return an.append(node);
            throw new IllegalArgumentException("Cannot only add to object or array node. Node found: " + parent.jsonString());
        });
    }

    /**
     * Insert an arbitrary object as a node into an array or object node.
     * @param path in form of a.b..c where {a,b,c} are field names in the JSON tree
     * @param value any object
     */
    default void insert(String path, Object value) {
        insert(path, JDux.node(value));
    }

    /**
     * Insert a collection of arbitrary elements as an array node to another array node.
     * @param path in form of a.b..c where {a,b,c} are field names in the JSON tree
     * @param values any collection
     */
    default void insert(String path, Collection<?> values) {
        ArrayNode arrayNode = () -> values.stream().map(JDux::node);
        insert(path, arrayNode);
    }

    /**
     * Delete the nodes at for the given path.
     */
    default void delete(String path) {
        JsonSelector selector = JsonPath.parse(path);
        JsonSelectorSegment tail = selector.tail();
        if (tail.depth() != JsonDepth.CHILD)
            throw new IllegalArgumentException("Can only delete fields from object");
        String parent = selector.previous().toString();
        update(parent, node -> {
            if (node instanceof ObjectNode on)
                return on.subset(child -> !child.label().equals(tail.key().orElseThrow(() -> new IllegalArgumentException("Expected key matcher"))));
            throw new IllegalArgumentException("Cannot update non-object node");
        });
    }

    /**
     * Assign the nodes for the given path to the supplied value.
     * @param path  in form of a.b..c where {a,b,c} are field names in the JSON tree
     * @param value any object type that can be translated to a JSON node
     */
    default void update(String path, Object value) {
        update(path, JDux.node(value));
    }

    /**
     * Assign the nodes for the given path.
     * @param path  in form of a.b..c where {a,b,c} are field names in the JSON tree
     * @param node  any JSON node
     */
    default void update(String path, JsonNode node) {
        update(path, n -> node);
    }

    /**
     * Replace nodes using the given function.
     * @param path   in form of a.b..c where {a,b,c} are field names in the JSON tree
     * @param update operator to replace the matched nodes with the return value
     */
    void update(String path, UnaryOperator<JsonNode> update);

    /**
     * Fetch a single node from the database.  If there are multiple, this will return the first match.
     * @param path  in form of a.b..c where {a,b,c} are field names in the JSON tree
     */
    default JsonNode lookup(String path) {
        return select(path).findFirst()
                .orElse(new JsonNode.NullNode());
    }

    /**
     * Query the JSON tree for the given path.
     * @param path in form of a.b..c where {a,b,c} are field names in the JSON tree
     * @return     A stream of matching nodes.
     */
    default Stream<JsonNode> select(String path) {
        return select(JsonPath.parse(path));
    }

    default Stream<JsonNode> select(JsonSelector selector) {
        var generation = singletonList(root());
        if (selector == JsonSelector.IDENTITY)
            return generation.stream();
        for (JsonSelectorSegment segment : selector) {
            generation = switch (segment.depth()) {
                case CHILD -> generation.stream().flatMap(JsonNode::children).filter(segment).collect(toList());
                case DESCENDANT -> generation.stream().flatMap(JsonNode::descendents).filter(segment).collect(toList());
                case IDENTITY -> throw new IllegalStateException();
            };
        }
        return generation.stream().map(n -> {
            if (n instanceof JsonNode.LabelledNode ln)
                return ln.unlabelled();
            return n;
        });
    }

    /**
     * Chain method to isolate a subset of nodes.
     * @param path in form of a.b..c where {a,b,c} are field names in the JSON tree
     * @return     A subject for updates / subscriptions on a subset of nodes.
     */
    default JsonSubject subject(String path) {
        return new JsonSubject() {
            @Override
            public void update(JsonNode value) {
                JsonDB.this.update(path, value);
            }
            @Override
            public void update(UnaryOperator<JsonNode> update) {
                JsonDB.this.update(path, update);
            }
            @Override
            public void subscribe(Consumer<JsonNode> consumer) {
                JsonDB.this.subscribe(path, consumer);
            }
            @Override
            public JsonNode get() {
                return JsonDB.this.select(path).findFirst().orElse(null);
            }
        };
    }

    /**
     * Get the root of the JSON tree.
     * @return the root of the JSON tree.
     */
    JsonNode root();

    /**
     * Set the root of the JSON tree.
     */
    JsonDB root(JsonNode newRoot);

    /**
     * Set the schema for update validation.
     */
    JsonDB schema(JsonSchema schema);

    /**
     * Get the current schema.
     */
    JsonSchema schema();

}
