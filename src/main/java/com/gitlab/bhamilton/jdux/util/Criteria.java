package com.gitlab.bhamilton.jdux.util;

import java.util.function.Predicate;

public interface Criteria<E> extends Predicate<E> {

    static <E> Criteria<E> create(Predicate<E> predicate,
                                  Predicate<Criteria<E>> intersects,
                                  Predicate<Criteria<E>> contains) {
        return new Criteria<E>() {
            @Override
            public boolean test(E e) {
                return predicate.test(e);
            }
            @Override
            public boolean intersects(Criteria<E> other) {
                return intersects.test(other);
            }
            @Override
            public boolean contains(Criteria<E> other) {
                return contains.test(other);
            }
        };
    }

    boolean intersects(Criteria<E> other);

    boolean contains(Criteria<E> other);

    default Criteria<E> or(Criteria<E> or) {
        if (contains(or))
            return this;
        else if (or.contains(this))
            return or;
        Criteria<E> self = this;
        return new Criteria<>() {
            @Override
            public boolean intersects(Criteria<E> other) {
                return self.intersects(other) || or.intersects(other);
            }
            @Override
            public boolean contains(Criteria<E> other) {
                return self.contains(other) || or.contains(other);
            }
            @Override
            public boolean test(E e) {
                return self.test(e) || or.test(e);
            }
            @Override
            public String toString() {
                return self.toString() + " | " + or.toString();
            }
        };
    }

    default Criteria<E> and(Criteria<E> and) {
        if (contains(and))
            return this;
        else if (and.contains(this))
            return and;
        Criteria<E> self = this;
        return new Criteria<>() {
            @Override
            public boolean intersects(Criteria<E> other) {
                return self.intersects(other) && and.intersects(other);
            }
            @Override
            public boolean contains(Criteria<E> other) {
                return self.contains(other) && and.contains(other);
            }
            @Override
            public boolean test(E e) {
                return self.test(e) && and.test(e);
            }
            @Override
            public String toString() {
                return self.toString() + " & " + and.toString();
            }
        };
    }

}
