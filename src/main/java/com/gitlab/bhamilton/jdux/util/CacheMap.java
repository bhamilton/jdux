package com.gitlab.bhamilton.jdux.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.stream.Stream;

public interface CacheMap<K, V> extends EntryPairs<K, V> {

    /**
     * Lazily cache items into a hashmap.
     */
    static <K extends Serializable, V> CacheMap<K, V> hashCache(CacheMap<K, V> delegate) {
        Map<K, V> map = new HashMap<>();
        return new CacheMap<>() {
            @Override
            public V get(K key) {
                V value = map.get(key);
                if (value == null) {
                    value = delegate.get(key);
                    map.put(key, value);
                }
                return value;
            }
            @Override
            public boolean contains(K key) {
                return map.containsKey(key) || delegate.contains(key);
            }
            @Override
            public void put(K key, V value) {
                map.put(key, value);
                delegate.put(key, value);
            }
            @Override
            public void remove(K key) {
                map.remove(key);
                delegate.remove(key);
            }
            @Override
            public void removeAll() {
                map.clear();
                delegate.removeAll();
            }
            @Override
            public Stream<K> keys() {
                return delegate.keys();
            }
            @Override
            public <E> Stream<E> entries(BiFunction<K, V, E> mappingFunction) {
                if (map.size() == delegate.size())
                    return map.entrySet().stream().map(e -> mappingFunction.apply(e.getKey(), e.getValue()));
                return delegate.entries((key, value) -> {
                    map.put(key, value);
                    return mappingFunction.apply(key, value);
                });
            }
            @Override
            public void entries(BiConsumer<K, V> biConsumer) {
                if (map.size() == delegate.size())
                    map.forEach(biConsumer);
                else {
                    delegate.entries((key, value) -> {
                        map.put(key, value);
                        biConsumer.accept(key, value);
                    });
                }
            }
            @Override
            public int size() {
                return delegate.size();
            }
        };
    }

    V get(K key);

    Stream<K> keys();

    boolean contains(K key);

    void put(K key, V value);

    void remove(K key);

    void removeAll();

    int size();

}
