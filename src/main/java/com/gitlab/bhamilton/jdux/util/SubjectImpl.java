package com.gitlab.bhamilton.jdux.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

class SubjectImpl<E> implements Subject<E> {

    private E value;
    private List<Consumer<E>> subscribers;

    public SubjectImpl(E value) {
        this.value = value;
        this.subscribers = new ArrayList<>();
    }

    @Override
    public void update(E value) {
        this.value = value;
        subscribers.forEach(c -> c.accept(value));
    }

    @Override
    public void update(UnaryOperator<E> update) {
        value = update.apply(value);
        subscribers.forEach(c -> c.accept(value));
    }

    @Override
    public void subscribe(Consumer<E> consumer) {
        subscribers.add(consumer);
    }

    @Override
    public E get() {
        return value;
    }

}
