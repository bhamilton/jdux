package com.gitlab.bhamilton.jdux.util;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.stream.Stream;

public interface EntryPairs<K, V> {

    <E> Stream<E> entries(BiFunction<K, V, E> mappingFunction);

    void entries(BiConsumer<K, V> biConsumer);

}
