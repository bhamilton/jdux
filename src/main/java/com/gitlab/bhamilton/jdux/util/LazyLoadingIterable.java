package com.gitlab.bhamilton.jdux.util;

public interface LazyLoadingIterable<E> extends Iterable<E>, LazyLoading {
}
