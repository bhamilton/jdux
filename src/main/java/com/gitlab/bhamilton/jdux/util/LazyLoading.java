package com.gitlab.bhamilton.jdux.util;

public interface LazyLoading {
    void load();
}
