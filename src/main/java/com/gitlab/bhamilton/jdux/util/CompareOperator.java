package com.gitlab.bhamilton.jdux.util;

import com.gitlab.bhamilton.jdux.JsonNode;
import com.gitlab.bhamilton.jdux.JsonParseException;
import com.gitlab.bhamilton.jdux.ObjectNode;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.IntPredicate;

import static java.util.stream.Collectors.toMap;

// TODO intersect / contains
@SuppressWarnings("unchecked")
public enum CompareOperator {

    EQUALS("=", JsonNode::equals),
    NOT_EQUALS("!=", EQUALS.check.negate()),
    LESS_THAN("<", inequality(i -> i > 0)),
    LESS_THAN_EQ("<=", inequality(i -> i >= 0)),
    GREATER_THAN(">", inequality(i -> i < 0)),
    GREATER_THAN_EQ(">=", inequality(i -> i <= 0)),
    REGEX_MATCH("=~", (value, node) ->
        value instanceof JsonNode.StringNode matchOn
            && node instanceof JsonNode.StringNode sn
            && sn.value().matches(matchOn.value())),
    IN("in", (array, node) -> array.children().anyMatch(node::equals)),
    NOT_IN("nin", IN.check.negate()),
    CONTAINS("contains", (value, node) -> node.children().findAny().isPresent() && node.children().anyMatch(value::equals)),
    SUBSET_OF("subsetof", (array, node) -> node.children().findAny().isPresent() && node.children().allMatch(child -> array.children().anyMatch(child::equals))),
    ANY_OF("anyof", (array, node) -> node.children().anyMatch(child -> array.children().anyMatch(child::equals))),
    NONE_OF("noneof", ANY_OF.check.negate());

    private final String stringValue;
    private final BiPredicate<JsonNode, JsonNode> check;

    CompareOperator(String stringValue, BiPredicate<JsonNode, JsonNode> check) {
        this.stringValue = stringValue;
        this.check = check;
    }

    public String stringValue() {
        return stringValue;
    }

    private final static Map<String, CompareOperator> LOOKUP = Arrays.stream(CompareOperator.values())
        .collect(toMap(CompareOperator::stringValue, Function.identity()));

    private static BiPredicate<JsonNode, JsonNode> inequality(IntPredicate intPredicate) {
        Comparator<JsonNode> comparator = Comparator.comparing(JsonNode::value);
        return (n1, n2) -> intPredicate.test(comparator.compare(n1, n2));
    }

    public static CompareOperator parse(String str) {
        CompareOperator compareOperator = LOOKUP.get(str.toLowerCase());
        if (compareOperator == null)
            throw new JsonParseException(String.format("Invalid comparison '%s'", str));
        return compareOperator;
    }

    public static boolean isCompareChar(int c) {
        return switch (c) {
            case '=', '!', '<', '>', '~' -> true;
            default -> false;
        };
    }

    public Criteria<JsonNode> getCriteria(String key, JsonNode value) {
        return new CompareCriteria(key, this, value, check);
    }

    public record CompareCriteria(
            String key,
            CompareOperator compare,
            JsonNode value,
            BiPredicate<JsonNode, JsonNode> check) implements Criteria<JsonNode> {

        @Override
        public boolean test(JsonNode node) {
            if (node instanceof ObjectNode objectNode)
                return check.test(value, objectNode.get(key));
            return false;
        }
        @Override
        public boolean intersects(Criteria<JsonNode> other) {
            return this.equals(other); // TODO
        }
        @Override
        public boolean contains(Criteria<JsonNode> other) {
            return this.equals(other); // TODO
        }

        @Override
        public String toString() {
            return key + ' ' + compare.stringValue() + ' ' + value.jsonString();
        }
    }
}
