package com.gitlab.bhamilton.jdux;

import java.util.Iterator;

public class JsonNodeChain implements Iterable<JsonNode> {

    private final JsonNodeChain previous;
    private final JsonNode node;
    private JsonNodeChain next;

    public JsonNodeChain(JsonNode node) {
        this(null, node);
    }

    public JsonNodeChain(JsonNodeChain previous,
                         JsonNode node) {
        this.previous = previous;
        this.node = node;
    }

    public JsonNodeType type() {
        return node.type();
    }

    public String label() {
        if (node instanceof JsonNode.LabelledNode ln)
            return ln.label();
        return null;
    }

    public JsonNode node() {
        return node;
    }

    public JsonNodeChain next(JsonNode node) {
        return next = new JsonNodeChain(this, node);
    }

    public JsonNodeChain root() {
        JsonNodeChain n = this;
        while (n.previous != null)
            n = n.previous;
        return n;
    }

    public JsonNodeChain next() {
        return next;
    }

    @Override
    public Iterator<JsonNode> iterator() {
        return new Iterator<>() {
            JsonNodeChain next = root();
            @Override
            public boolean hasNext() {
                return next != null;
            }
            @Override
            public JsonNode next() {
                JsonNodeChain current = next;
                next = current.next;
                return current.node;
            }
        };
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (JsonNode node : this) {
            if (node instanceof JsonNode.LabelledNode ln)
                sb.append(ln.label());
            else
                sb.append('/');
        }
        return sb.toString();
    }

}
