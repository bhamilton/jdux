package com.gitlab.bhamilton.jdux.db;

import com.gitlab.bhamilton.jdux.JsonDB;
import com.gitlab.bhamilton.jdux.JsonNode;
import com.gitlab.bhamilton.jdux.JsonNodeType;
import com.gitlab.bhamilton.jdux.JsonSchema;
import com.gitlab.bhamilton.jdux.JsonSchemaException;
import com.gitlab.bhamilton.jdux.JsonSelector;
import com.gitlab.bhamilton.jdux.ObjectNode;
import com.gitlab.bhamilton.jdux.util.CacheMap;

import java.util.HashSet;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

import static java.util.stream.Collectors.toCollection;

public class RoutingJsonDB implements JsonDB {

    private final CacheMap<String, JsonDB> routes;
    private JsonSchema schema;

    public RoutingJsonDB(CacheMap<String, JsonDB> routes) {
        this.routes = routes;
    }

    @Override
    public void update(String pathStr, UnaryOperator<JsonNode> update) {
        JsonSelector path = JsonPath.parse(pathStr);
        switch (path.depth()) {
            case CHILD -> routes.get(path.key().orElseThrow(this::keySelectorExpected)).update(path.next().toString(), update);
            case DESCENDANT -> routes.entries((key, db) -> {
                // when child matches descendant key, perform two updates
                if (key.equals(path.key()))
                    db.update(path.next().toString(), update);
                db.update(pathStr, update);
            });
            case IDENTITY -> root(update.apply(root()));
        }
    }

    @Override
    public void subscribe(String pathStr, Consumer<JsonNode> consumer) {
        JsonSelector path = JsonPath.parse(pathStr);
        switch (path.depth()) {
            case CHILD -> {
                JsonSelector next = path.next();
                routes.get(path.key().orElseThrow(this::keySelectorExpected)).subscribe(next.toString(), consumer);
            }
            case DESCENDANT -> routes.entries((key, db) -> {
                if (key.equals(path.key()))
                    db.subscribe(path.next().toString(), consumer);
                db.subscribe(pathStr, consumer);
            });
        }
    }

    private IllegalArgumentException keySelectorExpected() {
        throw new IllegalArgumentException("Only object key selector is allowed for root child here.");
    }

    @Override
    public ObjectNode root() {
        return () -> routes.entries(this::asNode);
    }

    private JsonNode.LabelledNode asNode(String key, JsonDB db) {
        return new LabelledNodeDecorator(key, db.root());
    }

    @Override
    public RoutingJsonDB root(JsonNode newRoot) {
        if (newRoot instanceof ObjectNode objectNode) {
            HashSet<String> keys = routes.keys().collect(toCollection(HashSet::new));
            objectNode.children()
                .forEach(child -> {
                    String key = child.label();
                    keys.remove(key);
                    routes.get(key).root(child.unlabelled());
                });
            // remove non-matching
            keys.forEach(routes::remove);
            return this;
        }
        throw new IllegalArgumentException("RoutingJsonDB requires ObjectNode as root");
    }

    @Override
    public JsonDB schema(JsonSchema schema) {
        if (schema.type() != JsonNodeType.object)
            throw new JsonSchemaException("Expected object type schema but was " + schema.type());
        schema.properties().forEach((property, propertySchema) -> {
            routes.get(property).schema(propertySchema);
        });
        this.schema = schema;
        return this;
    }

    @Override
    public JsonSchema schema() {
        return schema;
    }
}
