package com.gitlab.bhamilton.jdux.db;

import com.gitlab.bhamilton.jdux.ArrayNode;
import com.gitlab.bhamilton.jdux.JsonNode;
import com.gitlab.bhamilton.jdux.JsonNodeChain;
import com.gitlab.bhamilton.jdux.JsonNodeType;
import com.gitlab.bhamilton.jdux.JsonNodeUpdater;
import com.gitlab.bhamilton.jdux.JsonSchema;
import com.gitlab.bhamilton.jdux.JsonSchemaValidator;
import com.gitlab.bhamilton.jdux.JsonSelector;
import com.gitlab.bhamilton.jdux.ObjectNode;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import static com.gitlab.bhamilton.jdux.util.Iterables.filter;
import static com.gitlab.bhamilton.jdux.util.Iterables.filterMap;
import static com.gitlab.bhamilton.jdux.JsonDepth.DESCENDANT;
import static com.gitlab.bhamilton.jdux.util.Shorthands.then;
import static java.util.stream.Collectors.toList;

public class LocalJsonNodeUpdater implements JsonNodeUpdater {

    private final Collection<JsonUpdateSubscriber> allSubscribers = new ArrayList<>();
    private JsonSchemaValidator validator = chain -> {};

    private record JsonUpdateSubscriber(JsonSelector selection, Consumer<JsonNode> onUpdate) {
        boolean isDescendant() {
            return selection.depth() == DESCENDANT;
        }
        boolean accepts(JsonNode n) {
            return isDescendant() || selection.test(n);
        }
        JsonUpdateSubscriber next(JsonNode n) {
            return selection.test(n)
                ? new JsonUpdateSubscriber(selection.next(), onUpdate)
                : this;
        }
    }

    @Override
    public void applySchema(JsonSchema newSchema) {
        validator = newSchema;
    }

    @Override
    public JsonNode applyUpdate(String pathStr, JsonNode node, UnaryOperator<JsonNode> update) {
        var path = JsonPath.parse(pathStr);
        if (path == JsonSelector.IDENTITY) {
            JsonNode updatedRoot = update.apply(node);
            validator.validate(updatedRoot);
            filter(allSubscribers, s -> s.selection == JsonSelector.IDENTITY)
                .forEach(s -> s.onUpdate.accept(updatedRoot));
            return updatedRoot;
        }
        var superSetSubscribers = allSubscribers.stream().filter(s -> s.selection.contains(path)).collect(toList());
        if (!superSetSubscribers.isEmpty())
            update = then(update, result -> superSetSubscribers.forEach(s -> s.onUpdate.accept(result)));
        var subsetSubscribers = filter(allSubscribers, s -> path.contains(s.selection) && !superSetSubscribers.contains(s));

        return updateNode(node, path, subsetSubscribers, update);
    }


    @Override
    public void subscribe(String query, Consumer<JsonNode> consumer) {
        allSubscribers.add(new JsonUpdateSubscriber(JsonPath.parse(query), consumer));
    }

    private <N extends JsonNode> N updateNode(N node,
                                              JsonSelector pick,
                                              Iterable<JsonUpdateSubscriber> subscribers,
                                              UnaryOperator<JsonNode> update) {
        return updateNode(new JsonNodeChain(node), node, pick, subscribers, update);
    }

    @SuppressWarnings("unchecked")
    private <N extends JsonNode> N updateNode(JsonNodeChain chain,
                                              N node,
                                              JsonSelector pick,
                                              Iterable<JsonUpdateSubscriber> subscribers,
                                              UnaryOperator<JsonNode> update) {
        if (node instanceof JsonNode.LabelledNode ln)
            return (N) new LabelledNodeDecorator(ln.label(), updateNode(chain, ln.unlabelled(), pick, subscribers, update));
        else if (node instanceof ObjectNode on)
            return (N) new JsonUpdateObjectNode(chain, on, pick, subscribers, update);
        else if (node instanceof ArrayNode an)
            return (N) new JsonUpdateArrayNode(chain, an, pick, subscribers, update);
        return (N) new JsonUpdateNode<>(chain, node, pick, subscribers, update);
    }

    private static Iterable<JsonUpdateSubscriber> nextGen(Iterable<JsonUpdateSubscriber> subscribers, JsonNode node) {
        return filterMap(subscribers, s -> s.accepts(node), s -> s.next(node));
    }

    private static UnaryOperator<JsonNode> applyLabel(UnaryOperator<JsonNode> base) {
        return n -> {
            var result = base.apply(n);
            if (n instanceof JsonNode.LabelledNode ln && !(result instanceof JsonNode.LabelledNode))
                result = new LabelledNodeDecorator(ln.label(), result);
            return result;
        };
    }

    private class JsonUpdateArrayNode extends JsonUpdateNode<ArrayNode> implements ArrayNode {
        public JsonUpdateArrayNode(JsonNodeChain chain, ArrayNode node, JsonSelector pick, Iterable<JsonUpdateSubscriber> subscribers, UnaryOperator<JsonNode> update) {
            super(chain, node, pick, subscribers, update);
        }
    }

    private class JsonUpdateObjectNode extends JsonUpdateNode<ObjectNode> implements ObjectNode {
        public JsonUpdateObjectNode(JsonNodeChain chain, ObjectNode node, JsonSelector pick, Iterable<JsonUpdateSubscriber> subscribers, UnaryOperator<JsonNode> update) {
            super(chain, node, pick, subscribers, applyLabel(update));
        }
        @Override
        public Iterator<? extends LabelledNode> childIterator() {
            return children().iterator();
        }
        @Override
        public Stream<? extends LabelledNode> children() {
            return node.children()
                .map(this::decorateChild)
                .map(LabelledNode.class::cast);
        }
        @Override
        public ObjectNode put(ObjectNode other) {
            return node.put(other);
        }
        @Override
        public ObjectNode put(String label, JsonNode other) {
            return node.put(label, other);
        }
        @Override
        public JsonNode get(String key) {
            return node.get(key);
        }
    }

    private class JsonUpdateNode<N extends JsonNode> implements JsonNode {

        final JsonNodeChain chain;
        final N node;
        final JsonSelector select;
        final Iterable<JsonUpdateSubscriber> subscribers;
        final UnaryOperator<JsonNode> update;

        public JsonUpdateNode(JsonNodeChain chain,
                              N node,
                              JsonSelector select,
                              Iterable<JsonUpdateSubscriber> subscribers,
                              UnaryOperator<JsonNode> update) {
            this.chain = chain;
            this.node = node;
            this.select = select;
            this.subscribers = subscribers;
            this.update = update;
        }

        @Override
        public JsonNodeType type() {
            return node.type();
        }

        // temporary node for writing updates, reflection not needed
        @Override
        public <E> E asA(Class<E> type) {
            throw new UnsupportedOperationException();
        }

        // temporary node for writing updates, reflection not needed
        @Override
        public Object asA(Type type) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Stream<? extends JsonNode> children() {
            return node.children().map(this::decorateChild);
        }

        protected JsonNode decorateChild(JsonNode n) {
            if (select.test(n))
                return select.hasNext()
                    ? updateNode(chain.next(n), n, select.next(), nextGen(subscribers, n), update)
                    : doUpdate(n);
            else if (select.depth() == DESCENDANT)
                return updateNode(chain.next(n), n, select, nextGen(subscribers, n), update);
            else
                return n;
        }

        @Override
        public boolean isLeaf() {
            return node.isLeaf();
        }

        @Override
        public String toString() {
            return node.jsonString();
        }

        protected JsonNode doUpdate(JsonNode n) {
            JsonNode result = n instanceof LabelledNode ln
                ? LabelledNode.labelled(ln.label(), update.apply(ln.unlabelled()))
                : update.apply(n);
            validator.validate(chain.next(result).root());
            subscribers.forEach(s -> s.onUpdate.accept(result));
            return result;
        }

    }

}
