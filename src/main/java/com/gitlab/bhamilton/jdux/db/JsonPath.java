package com.gitlab.bhamilton.jdux.db;

import com.gitlab.bhamilton.jdux.JsonDepth;
import com.gitlab.bhamilton.jdux.JsonNode;
import com.gitlab.bhamilton.jdux.JsonParseException;
import com.gitlab.bhamilton.jdux.JsonSelector;
import com.gitlab.bhamilton.jdux.JsonSelectorSegment;
import com.gitlab.bhamilton.jdux.util.CompareOperator;
import com.gitlab.bhamilton.jdux.util.Criteria;

import java.util.Iterator;
import java.util.Optional;

import static com.gitlab.bhamilton.jdux.JsonSelectorSegment.IDENTITY;

public class JsonPath {

    private static final JsonParser jsonParser = new JsonParser();

    public static JsonSelector parse(String str) {
        if (str == null || str.isEmpty())
            return JsonSelector.IDENTITY;
        TextInput in = TextInput.wrap(str);
        JsonSelectorSegment segment = parseSegment(in);
        JsonPathLink link = new JsonPathLink(segment), head = link;
        while ((segment = parseSegment(in)) != IDENTITY)
            link = new JsonPathLink(link, segment);
        return head;
    }

    public static class JsonPathLink implements JsonSelector {

        private final JsonSelectorSegment segment;
        private final JsonPathLink previous;
        private JsonPathLink next;

        public JsonPathLink(JsonSelectorSegment segment) {
            this(null, segment);
        }

        public JsonPathLink(JsonPathLink previous, JsonSelectorSegment segment) {
            this.previous = previous;
            this.segment = segment;
            if (previous != null)
                previous.next = this;
        }

        @Override
        public JsonSelectorSegment head() {
            return segment;
        }

        @Override
        public JsonSelector previous() {
            return previous == null ? JsonSelector.IDENTITY : previous;
        }

        @Override
        public JsonSelector next() {
            return next == null ? JsonSelector.IDENTITY : next;
        }

        @Override
        public boolean contains(Criteria<JsonNode> other) {
            if (other instanceof JsonSelector s)
                return s.stringValue().matches(stringValue().replaceAll("\\.\\.", ".*")); // TODO
            return false;
        }

        @Override
        public Iterator<JsonSelectorSegment> iterator() {
            return new Iterator<>() {
                JsonPathLink link = JsonPathLink.this;
                @Override
                public boolean hasNext() {
                    return link != null;
                }
                @Override
                public JsonSelectorSegment next() {
                    try {
                        return link.segment;
                    } finally {
                        link = link.next;
                    }
                }
            };
        }

        @Override
        public String toString() {
            return stringValue();
        }
    }

    public static JsonSelectorSegment parseSegment(TextInput in) {
        if (!in.hasNext())
            return IDENTITY;
        int depthChar = in.peek();
        if (in.index() == 0) // period is optional for start of query
            depthChar = depthChar != '[' ? '.' : depthChar;
        final JsonDepth depth = getDepth(in);
        final int peek = in.peek();
        switch (depthChar) {
            case '.' -> {
                return switch (peek) {
                    // yeah, this is how I do. deal with it
                    case 'q','w','e','r','t','y','u','i','o',
                        'p','a','s','d','f','g','h','j','k',
                        'l','z','x','c','v','b','n','m','Q',
                        'W','E','R','T','Y','U','I','O','P',
                        'A','S','D','F','G','H','J','K','L',
                        'Z','X','C','V','B','N','M' -> matchKey(depth, in.readWhile(Character::isLetterOrDigit));
                    case '*' -> allMatch(depth, in);
                    default -> throw unexpectedChar(in, peek);
                };
            }
            case '?' -> {
                return getCriteriaSegment(in, -1);
            }
            case '[' -> {
                return getCriteriaSegment(in, ']');
            }
            default -> throw unexpectedChar(in, peek);
        }
    }

    private static JsonParseException unexpectedChar(TextInput in, int peek) {
        return new JsonParseException(String.format("Unexpected character %c", peek), in);
    }

    private static JsonDepth getDepth(TextInput in) {
        final String depthStr = in.readWhile(ch -> ch == '.' || ch == '[' || ch == '?');
        return switch (depthStr) {
            case "",".","[","?" -> JsonDepth.CHILD;
            case ".." -> JsonDepth.DESCENDANT;
            default -> throw new JsonParseException(String.format("Unexpected depth operator '%s'", depthStr), in);
        };
    }


    public static JsonSelectorSegment matchKey(JsonDepth depth, String key) {
        return new KeyMatchSegment(depth, key);
    }

    private static CriteriaSegment getCriteriaSegment(TextInput in, int close) {
        var segment = getCriteriaClause(in);
        for (var ch = in.skipWhitespace().peek(); in.hasNext() && ch != close; ch = in.skipWhitespace().peek()) {
            segment = switch (ch) {
                case '|', ',' -> segment.or(getCriteriaClause(in.skip()));
                case '&' -> segment.and(getCriteriaClause(in.skip('&')));
                default -> throw unexpectedChar(in, ch);
            };
        }
        if (close != -1)
            in.skipWhitespace().skip((char) close);
        return segment;
    }

    private static CriteriaSegment getCriteriaClause(TextInput in) {
        int peek = in.skipWhitespace().peek();
        return switch (peek) {
            case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-' -> sliceClause(in);
            case 'q','w','e','r','t','y','u','i','o',
                'p','a','s','d','f','g','h','j','k',
                'l','z','x','c','v','b','n','m','Q',
                'W','E','R','T','Y','U','I','O','P',
                'A','S','D','F','G','H','J','K','L',
                'Z','X','C','V','B','N','M' -> comparisonClause(in);
            case '(' -> getCriteriaSegment(in.skip('('), ')');
            default -> throw unexpectedChar(in, peek);
        };
    }

    private static CriteriaSegment sliceClause(TextInput in) {
        int start = Integer.parseInt(in.readWhile(JsonPath::isIntCharacter));
        int end = start + 1;
        if (in.peek() == ':')
            end = Integer.parseInt(in.skip(':').readWhile(JsonPath::isIntCharacter));

        return new CriteriaSegment(JsonDepth.CHILD, new SliceCriteria(start, end));
    }

    public static CriteriaSegment comparisonClause(TextInput in) {
        String field = in.readWhile(Character::isLetterOrDigit);
        CompareOperator compareOperator = CompareOperator.parse(
            in.skipWhitespace().readWhile(Character.isLetter(in.skipWhitespace().peek())
            ? Character::isLetter
            : CompareOperator::isCompareChar));
        JsonNode value = jsonParser.parse(in.skipWhitespace());
        if (!value.isLeaf()) // load children if array or object
            value.children().forEach(child -> {});
        return new CriteriaSegment(JsonDepth.CHILD, compareOperator.getCriteria(field, value));
    }

    private static boolean isIntCharacter(int c) {
        return c == '-' || Character.isDigit(c);
    }

    public static JsonSelectorSegment allMatch(JsonDepth depth, TextInput in) {
        in.skip('*');
        return new BaseJsonPathSegment(depth) {
            @Override
            public String stringValue() {
                return "*";
            }
            @Override
            public boolean intersects(Criteria<JsonNode> other) {
                return true;
            }
            @Override
            public boolean contains(Criteria<JsonNode> other) {
                return true;
            }
            @Override
            public boolean test(JsonNode jsonNode) {
                return true;
            }
        };
    }

    static abstract class BaseJsonPathSegment implements JsonSelectorSegment {

        protected final JsonDepth depth;

        public BaseJsonPathSegment(JsonDepth depth) {
            this.depth = depth;
        }

        @Override
        public JsonDepth depth() {
            return depth;
        }

        @Override
        public Optional<String> key() {
            return Optional.empty();
        }

        @Override
        public String stringValue() {
            return depth.stringValue();
        }
    }

    private static class KeyMatchSegment extends BaseJsonPathSegment {
        private final String key;

        public KeyMatchSegment(JsonDepth depth, String key) {
            super(depth);
            this.key = key;
        }

        @Override
        public Optional<String> key() {
            return Optional.of(key);
        }

        @Override
        public String stringValue() {
            return super.stringValue() + key;
        }

        @Override
        public boolean contains(Criteria<JsonNode> other) {
            if (other instanceof JsonSelectorSegment s)
                return s.key().isPresent() && key.equalsIgnoreCase(s.key().get());
            return false;
        }

        @Override
        public boolean intersects(Criteria<JsonNode> other) {
            return contains(other);
        }

        @Override
        public boolean test(JsonNode node) {
            return node instanceof JsonNode.LabelledNode ln && ln.label().equals(key().orElseThrow());
        }
    }

    private static class CriteriaSegment extends BaseJsonPathSegment {

        private final Criteria<JsonNode> criteria;

        public CriteriaSegment(JsonDepth depth, Criteria<JsonNode> criteria) {
            super(depth);
            this.criteria = criteria;
        }

        @Override
        public boolean intersects(Criteria<JsonNode> other) {
            if (other instanceof CriteriaSegment cs)
                return criteria.intersects(cs.criteria);
            return criteria.intersects(other);
        }

        @Override
        public boolean contains(Criteria<JsonNode> other) {
            if (other instanceof CriteriaSegment cs)
                return criteria.contains(cs.criteria);
            return criteria.contains(other);
        }

        @Override
        public boolean test(JsonNode jsonNode) {
            return criteria.test(jsonNode);
        }

        @Override
        public String stringValue() {
            return criteria.toString();
        }

        @Override
        public CriteriaSegment or(Criteria<JsonNode> or) {
            return new CriteriaSegment(depth(), criteria.or(or));
        }

        @Override
        public CriteriaSegment and(Criteria<JsonNode> and) {
            return new CriteriaSegment(depth(), criteria.and(and));
        }

        @Override
        public String toString() {
            return criteria.toString();
        }
    }

    private static class SliceCriteria implements Criteria<JsonNode> {

        private final int start, end;

        public SliceCriteria(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public boolean intersects(Criteria<JsonNode> other) {
            if (other instanceof SliceCriteria s)
                return start >= s.start && start < s.end
                    || end > s.start && end <= s.end
                    || start <= s.start && end >= s.end;
            return true;
        }

        @Override
        public boolean contains(Criteria<JsonNode> other) {
            if (other instanceof SliceCriteria s)
                return start <= s.start && end >= s.end;
            return false;
        }

        @Override
        public boolean test(JsonNode node) {
            return false; // TODO index
        }

        @Override
        public String toString() {
            return String.format("[%d:%d]", start, end);
        }
    }
}
