package com.gitlab.bhamilton.jdux.db;

import com.gitlab.bhamilton.jdux.JsonDB;
import com.gitlab.bhamilton.jdux.JsonNode;
import com.gitlab.bhamilton.jdux.JsonNodeUpdater;
import com.gitlab.bhamilton.jdux.JsonSchema;

import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class StreamingJsonDB implements JsonDB {

    public interface StreamOptions<O extends Appendable> {

        TextInput input();

        O output();

        void after(Appendable output);

    }

    private final Supplier<TextInput> source;
    private final Supplier<Appendable> sink;
    private final Consumer<Appendable> after;
    private final JsonParser parser;
    private final JsonWriter writer;
    private final JsonNodeUpdater updater;
    private JsonSchema schema = JsonSchema.empty();

    public <O extends Appendable> StreamingJsonDB(StreamOptions<O> streamOptions,
                                                  JsonParser parser,
                                                  JsonWriter writer) {
        this(
                streamOptions::input,
                streamOptions::output,
                streamOptions::after,
                parser,
                writer
        );
    }

    public StreamingJsonDB(Supplier<TextInput> source,
                           Supplier<Appendable> sink,
                           Consumer<Appendable> after,
                           JsonParser parser,
                           JsonWriter writer) {
        this.source = source;
        this.sink = sink;
        this.after = after;
        this.parser = parser.getLookup() == EMPTY ? parser.withLookup(this) : parser;
        this.writer = writer;
        this.updater = new LocalJsonNodeUpdater();
    }

    @Override
    public void update(String query, UnaryOperator<JsonNode> update) {
        JsonNode root = root();
        JsonNode updatedNode = updater.applyUpdate(query, root, update);
        writeNode(updatedNode);
    }

    private void writeNode(JsonNode root) {
        var out = sink.get();
        writer.write(root, out);
        after.accept(out);
    }

    @Override
    public StreamingJsonDB root(JsonNode newRoot) {
        writeNode(newRoot);
        return this;
    }

    @Override
    public JsonNode root() {
        return parser.recall(true).parse(source.get());
    }

    @Override
    public JsonDB schema(JsonSchema schema) {
        this.schema = schema;
        updater.applySchema(schema);
        return this;
    }

    @Override
    public JsonSchema schema() {
        return schema;
    }

    private JsonNodeUpdater updater() {
        return updater;
    }

    @Override
    public void subscribe(String path, Consumer<JsonNode> consumer) {
        updater().subscribe(path, consumer);
    }

}
