package com.gitlab.bhamilton.jdux.db;

import com.gitlab.bhamilton.jdux.util.CacheMap;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

public class FolderCacheMap<K, V> implements CacheMap<K, V> {

    private final Path folder;
    private final Function<K, String> keyFileNameFunction;
    private final Function<String, K> fileNameKeyFunction;
    private final Function<Path, V> fileValueFunction;
    private final BiConsumer<Path, V> valueWriter;

    public FolderCacheMap(Path folder,
                          Function<K, String> keyFileNameFunction,
                          Function<String, K> fileNameKeyFunction,
                          Function<Path, V> fileValueFunction,
                          BiConsumer<Path, V> valueWriter) {
        this.folder = folder;
        this.keyFileNameFunction = keyFileNameFunction;
        this.fileNameKeyFunction = fileNameKeyFunction;
        this.fileValueFunction = fileValueFunction;
        this.valueWriter = valueWriter;
    }

    @Override
    public V get(K key) {
        return fileValueFunction.apply(getFile(key));
    }

    @Override
    public Stream<K> keys() {
        try {
            return Files.list(folder)
                .map(path -> fileNameKeyFunction.apply(path.getFileName().toString()));
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    @Override
    public boolean contains(K key) {
        return Files.exists(getFile(key));
    }

    @Override
    public void put(K key, V value) {
        valueWriter.accept(getFile(key), value);
    }

    @Override
    public void remove(K key) {
        try {
            Files.delete(getFile(key));
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    @Override
    public void removeAll() {
        try {
            Files.list(folder).forEach(this::deleteFile);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    private void deleteFile(Path path) {
        try {
            Files.delete(path);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    @Override
    public <E> Stream<E> entries(BiFunction<K, V, E> mappingFunction) {
        try {
            return Files.list(folder)
                .map(path -> mappingFunction.apply(fileNameKeyFunction.apply(path.getFileName().toString()), fileValueFunction.apply(path)));
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    @Override
    public void entries(BiConsumer<K, V> biConsumer) {
        try {
            Files.list(folder)
                .forEach(path -> biConsumer.accept(fileNameKeyFunction.apply(path.getFileName().toString()), fileValueFunction.apply(path)));
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    // TODO cache this!
    @Override
    public int size() {
        try {
            return (int) Files.list(folder).count();
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    private Path getFile(K key) {
        return folder.resolve(keyFileNameFunction.apply(key));
    }

}
