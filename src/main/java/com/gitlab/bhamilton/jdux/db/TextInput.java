package com.gitlab.bhamilton.jdux.db;

import com.gitlab.bhamilton.jdux.util.Pool;

import java.io.StringReader;
import java.nio.CharBuffer;
import java.util.function.IntPredicate;

import static java.lang.Character.toLowerCase;

/**
 * Simplifies reading in text data.  Allows for peeking and skipping.
 *
 * Note: it is recommended to wrap Readers and close upon end of use so that CharBuffers may be reused.
 */
public interface TextInput extends AutoCloseable {

    Pool<CharBuffer> BUFFER_POOL = Pool.create(() -> CharBuffer.allocate(1024));

    static TextInput wrap(String str) {
        return new BasicTextInput(new StringReader(str), CharBuffer.allocate(str.length()), () -> {});
    }

    static TextInput wrap(Readable in) {
        final CharBuffer buffer = BUFFER_POOL.get();
        return new BasicTextInput(in, buffer, () -> BUFFER_POOL.put(buffer));
    }

    int read();
    String read(int nchars);
    default String readRemaining() {
        final StringBuilder sb = new StringBuilder();
        while (hasNext())
            sb.append((char) read());
        return sb.toString();
    }
    default String readLine() {
        return readUntilSkipping('\n');
    }
    default String readUntil(IntPredicate predicate) {
        return readWhile(predicate.negate());
    }
    default String readUntil(char until) {
        return readWhile(c -> c != until);
    }
    default String readUntilSkipping(char until) {
        final String s = readWhile(c -> c != until);
        if (hasNext())
            skip();
        return s;
    }
    default String readUntilWithEscape(int until, int escape) {
        final StringBuilder sb = new StringBuilder();
        boolean escaped = false;
        int next;
        while (hasNext() && ((next = peek()) != until || escaped)) {
            sb.append((char) read());
            escaped = next == escape;
        }
//        while (hasNext() && (peek() != until || escaped)) {
//            escaped = peek() == escape;
//            sb.append((char) (escaped && hasNext() ? skip().read() : read()));
//        }
        return sb.toString();
    }
    default String readWhile(IntPredicate p) {
        final StringBuilder sb = new StringBuilder();
        while (hasNext() && p.test(peek()))
            sb.append((char) read());
        return sb.toString();
    }
    default String readUntil(CharSequence str) {
        final StringBuilder sb = new StringBuilder();
        while (hasNext()) {
            if (str.charAt(0) == peek() && str.equals(peek(str.length())))
                break;
            sb.append((char) read());
        }
        return sb.toString();
    }

    int peek();
    String peek(int length);
    String peekWhile(IntPredicate p);

    default TextInput skipIgnoreCase(char ch) {
        return skipIgnoreCase(new String(new char[]{ch}));
    }
    default TextInput skipIgnoreCase(String str) {
        int i=0;
        while (hasNext() && i < str.length()) {
            if (toLowerCase(str.charAt(i++)) != toLowerCase(peek()))
                throw new IllegalStateException("Expected \""+str.substring(i-1)+"\" but was "+((char) peek())
                        +" for input \""+this.toString()+"\".");
            read();
        }
        return this;
    }
    default TextInput skipWhitespace() {
        return skipWhile(Character::isWhitespace);
    }
    default TextInput skipUntil(int ch) {
        return skipWhile(c -> c != ch);
    }
    default TextInput skipUntil(CharSequence str) {
        while (hasNext()) {
            skipUntil(str.charAt(0));
            if (peek(str.length()).equals(str))
                return this;
        }
        throw new IllegalStateException("Expected \""+str+"\" but reached end of input.");
    }
    default TextInput skipWhile(IntPredicate p) {
        while (hasNext() && p.test(peek()))
            read();
        return this;
    }
    default TextInput skip(char ch) {
        int actual = read();
        if (hasNext() && actual != ch)
            throw new IllegalStateException(String.format("Expected %c but was %c", ch, actual));
        return this;
    }
    default TextInput skip(int count) {
        for (int i=0; i < count; i++)
            read();
        return this;
    }
    default TextInput skip() {
        read();
        return this;
    }

    @Override
    void close();

    long index();

    TextInput index(long index);

    boolean hasNext();
}
