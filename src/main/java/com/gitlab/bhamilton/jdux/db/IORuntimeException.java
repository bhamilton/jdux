package com.gitlab.bhamilton.jdux.db;

public class IORuntimeException extends RuntimeException {
    public IORuntimeException(Throwable cause) {
        super(cause);
    }
}
