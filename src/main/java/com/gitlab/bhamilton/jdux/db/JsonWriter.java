package com.gitlab.bhamilton.jdux.db;

import com.gitlab.bhamilton.jdux.ArrayNode;
import com.gitlab.bhamilton.jdux.JsonNode;
import com.gitlab.bhamilton.jdux.ObjectNode;

import java.io.IOException;
import java.util.Iterator;
import java.util.stream.Stream;

public class JsonWriter {

    private boolean pretty = false;
    private String indent = "  ";
    private int referenceExpansion = 0;

    public JsonWriter() {}

    public JsonWriter(boolean pretty, String indent, int referenceExpansion) {
        this.pretty = pretty;
        this.indent = indent;
        this.referenceExpansion = referenceExpansion;
    }

    public JsonWriter setPretty(boolean pretty) {
        this.pretty = pretty;
        return this;
    }

    public JsonWriter setIndent(String indent) {
        this.indent = indent;
        return this;
    }

    public JsonWriter withReferenceExpansion(int referenceExpansion) {
        return new JsonWriter(pretty, indent, referenceExpansion);
    }

    public String toString(JsonNode node) {
        StringBuilder sb = new StringBuilder();
        write(node, sb);
        return sb.toString();
    }

    public String toString(Stream<JsonNode> nodes) {
        StringBuilder sb = new StringBuilder();
        write(nodes, sb);
        return sb.toString();
    }

    public void write(JsonNode node, Appendable out) {
        write(0, node, out);
    }

    public void write(Stream<JsonNode> nodes, Appendable out) {
        writeCollection(0, nodes.iterator(), out, '[', ']');
    }

    private void write(int depth, JsonNode node, Appendable out) {
        if (node instanceof ArrayNode an)
            writeArray(depth, an, out);
        else if (node instanceof ObjectNode on)
            writeObject(depth, on, out);
        else if (node instanceof JsonNode.LabelledNode ln)
            writeLabelled(depth, ln, out);
        else if (node instanceof JsonNode.ReferenceNode rf)
            writeReference(depth, rf, out);
        else
            writeValue(node, out);
    }

    private void writeReference(int depth, JsonNode.ReferenceNode node, Appendable out) {
        if (referenceExpansion > 0)
            withReferenceExpansion(referenceExpansion - 1).write(depth, node.getReference(), out);
        else
            writeValue(node, out);
    }

    private void writeArray(int depth, ArrayNode an, Appendable out) {
        writeCollection(depth, an, out, '[', ']');
    }

    private void writeObject(int depth, ObjectNode on, Appendable out) {
        writeCollection(depth, on, out, '{', '}');
    }

    private void writeLabelled(int depth, JsonNode.LabelledNode ln, Appendable out) {
        try {
            out.append('"').append(ln.label()).append('"').append(':');
            if (pretty)
                out.append(' ');
            write(depth, ln.unlabelled(), out);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    private void writeValue(JsonNode node, Appendable out) {
        try {
            out.append(node.toString());
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    private void writeCollection(int depth, JsonNode node, Appendable out, char open, char close) {
        writeCollection(depth, node.childIterator(), out, open, close);
    }

    private void writeCollection(int depth,
                                 Iterator<? extends JsonNode> children,
                                 Appendable out,
                                 char open,
                                 char close) {
        try {
            out.append(open);
            if (!children.hasNext()) {
                out.append(close);
                return;
            }
            depth++;
            spacing(depth, out).write(depth, children.next(), out);
            while (children.hasNext())
                spacing(depth, out.append(',')).write(depth, children.next(), out);
            spacing(depth - 1, out);
            out.append(close);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    private JsonWriter spacing(int depth, Appendable out) throws IOException {
        if (pretty) {
            out.append(System.lineSeparator());
            for (int i = 0; i < depth; i++)
                out.append(indent);
        }
        return this;
    }

}
