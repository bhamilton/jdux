package com.gitlab.bhamilton.jdux;

public interface JsonSchemaValidator {

    default void validate(JsonNode node) {
        validate(new JsonNodeChain(node));
    }
    default void validate(JsonNode... nodes) {
        if (nodes.length == 0)
            return;
        JsonNodeChain chain = new JsonNodeChain(nodes[0]), root = chain;
        for (int i=1; i < nodes.length; i++)
            chain = chain.next(nodes[i]);
        validate(root);
    }

    void validate(JsonNodeChain chain);

}
