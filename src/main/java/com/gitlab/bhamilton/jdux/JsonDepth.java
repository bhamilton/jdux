package com.gitlab.bhamilton.jdux;

public enum JsonDepth {
    IDENTITY(""),
    CHILD("."),
    DESCENDANT("..");

    final String stringValue;

    JsonDepth(String stringValue) {
        this.stringValue = stringValue;
    }

    public String stringValue() {
        return stringValue;
    }
}
