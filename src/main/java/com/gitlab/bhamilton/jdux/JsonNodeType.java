package com.gitlab.bhamilton.jdux;

public enum JsonNodeType {
    array,
    bool,
    nil,
    number,
    object,
    ref,
    string
}
