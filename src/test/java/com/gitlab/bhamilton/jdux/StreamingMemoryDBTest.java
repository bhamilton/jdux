package com.gitlab.bhamilton.jdux;

public class StreamingMemoryDBTest extends AbstractJsonDBTest {

    @Override
    protected JsonDB getDB(String key) {
        return JDux.memDB(1024 * 1024);
    }

}
