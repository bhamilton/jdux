package com.gitlab.bhamilton.jdux;

import java.time.Instant;
import java.util.List;

public record User(
    int id,
    String name,
    Instant lastLogin,
    List<Role> roles
) {}
