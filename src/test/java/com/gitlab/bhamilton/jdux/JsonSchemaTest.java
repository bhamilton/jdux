package com.gitlab.bhamilton.jdux;

import org.junit.Test;

import static com.gitlab.bhamilton.jdux.JDux.parse;
import static com.gitlab.bhamilton.jdux.JsonNode.LabelledNode.labelled;
import static org.junit.Assert.*;

public class JsonSchemaTest {

    private static final String PERSON_SCHEMA = """
        {
          "type": "object",
          "properties": {
            "firstName": {
              "type": "string"
            },
            "lastName": {
              "type": "string"
            },
            "age": {
              "type": "number"
            }
          }
        }
        """;
    private static final String BOOK_SCHEMA = String.format("""
        {
          "type": "object",
          "properties": {
            "books": {
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "title": {
                    "type": "string"
                  },
                  "author": {
                    "type": "ref",
                    "path": "/authors"
                  }
                }
              }
            },
            "authors": {
              "type": "array",
              "items": %s
            }
          }
        }
        """, PERSON_SCHEMA);

    @Test
    public void acceptAll() {
        JsonSchema schema = parseSchema("{}");
        schema.validate(parse("true"));
        schema.validate(parse("123"));
        schema.validate(parse("'foo'"));
        schema.validate(parse("{'foo':'bar'}"));
        schema.validate(parse("[{'foo':123}]"));
    }

    @Test
    public void personSchema() {
        JsonSchema schema = parseSchema(PERSON_SCHEMA);
        JsonNode personJson = parse("""
            {
              "firstName": "Bob",
              "lastName": "Loblaw",
              "age": 45
            }
            """);
        schema.validate(personJson);
        personJson.children().forEach(child -> schema.validate(personJson, child));
        try {
            schema.validate(personJson, labelled("firstName", new JsonNode.NumberNode(123)));
        } catch (JsonSchemaException e) {
            return;
        }
        fail("Expected validation error.");
    }

    @Test
    public void references() {
        JsonSchema schema = parseSchema(BOOK_SCHEMA);
        JsonNode booksJson = parse("""
            {
              "books": [{
                "title": "1984",
                "author": "#authors?firstName='George'&lastName='Orwell'"
              }],
              "authors": [{
                "firstName": "George",
                "lastName": "Orwell"
              }]
            }
            """);
        schema.validate(booksJson);
    }

    public JsonSchema parseSchema(String str) {
        return parse(str).asA(JsonSchema.class);
    }

}
