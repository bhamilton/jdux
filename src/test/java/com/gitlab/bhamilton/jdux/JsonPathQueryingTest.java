package com.gitlab.bhamilton.jdux;

import org.junit.BeforeClass;
import org.junit.Test;

import static java.util.stream.Collectors.joining;
import static org.junit.Assert.assertEquals;

public class JsonPathQueryingTest {

    private static JsonDB db;

    @BeforeClass
    public static void setUp() {
        db = TestUtils.dbFromResource("/examples/books.json");
    }

    @Test
    public void equals() {
        assertEquals("""
            Learning JavaScript Design Patterns
            Speaking JavaScript
            Programming JavaScript Applications
            You Don't Know JS
            Git Pocket Guide
            Designing Evolvable Web APIs with ASP.NET
            """.trim(), findAndJoin("books[publisher=\"O'Reilly Media\"].title"));
    }

    @Test
    public void notEquals() {
        assertEquals("""
            Eloquent JavaScript, Second Edition
            Understanding ECMAScript 6
            """.trim(), findAndJoin("books[publisher!=\"O'Reilly Media\"].title"));
    }

    @Test
    public void ranges() {
        String expected = """
            Eloquent JavaScript, Second Edition
            Speaking JavaScript
            Understanding ECMAScript 6
            Designing Evolvable Web APIs with ASP.NET
            """;
        assertEquals(expected.trim(), findAndJoin("books[pages > 300 & pages < 600].title"));
        assertEquals(expected.trim(), findAndJoin("books[pages>=352 & pages<=538].title"));
    }

    @Test
    public void regex() {
        assertEquals("""
            Eloquent JavaScript, Second Edition
            Learning JavaScript Design Patterns
            Speaking JavaScript
            Programming JavaScript Applications
            """.trim(), findAndJoin("books[title=~'.*Java.*'].title"));
    }

    @Test
    public void in() {
        String expected = """
            Eloquent JavaScript, Second Edition
            Understanding ECMAScript 6
            """;
        assertEquals(expected.trim(), findAndJoin("books[publisher in ['No Starch Press']].title"));
        assertEquals(expected.trim(), findAndJoin("books[publisher nin [\"O'Reilly Media\"]].title"));
    }

    @Test
    public void nesting() {
        assertEquals("""
            Eloquent JavaScript, Second Edition
            Learning JavaScript Design Patterns
            Speaking JavaScript
            Programming JavaScript Applications
            """.trim(), findAndJoin("books[title=~'.*Java.*' & (publisher=\"O'Reilly Media\" | pages > 300)].title"));
    }

    @Test
    public void subset() {
        assertEquals("""
            Learning JavaScript Design Patterns
            Programming JavaScript Applications
            """.trim(), findAndJoin("books[authors subsetof [\"Addy Osmani\",\"Eric Elliott\"]].title"));
    }

    @Test
    public void anyof() {
        assertEquals("""
            Learning JavaScript Design Patterns
            Programming JavaScript Applications
            """.trim(), findAndJoin("books[authors anyof [\"Addy Osmani\",\"Eric Elliott\"]].title"));
    }

    @Test
    public void noneof() {
        assertEquals("""
            Eloquent JavaScript, Second Edition
            Speaking JavaScript
            Understanding ECMAScript 6
            You Don't Know JS
            Git Pocket Guide
            Designing Evolvable Web APIs with ASP.NET
            """.trim(), findAndJoin("books[authors noneof [\"Addy Osmani\",\"Eric Elliott\"]].title"));
    }

    @Test
    public void contains() {
        assertEquals("""
            Designing Evolvable Web APIs with ASP.NET
            """.trim(), findAndJoin("books[authors contains \"Darrel Miller\"].title"));
    }

    public String findAndJoin(String select) {
        return db.select(select)
            .map(n -> n.asA(String.class))
            .collect(joining("\n"));
    }

}
