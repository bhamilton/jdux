package com.gitlab.bhamilton.jdux;

import org.junit.BeforeClass;
import org.junit.Test;

import java.time.Instant;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

public class JsonNodeTest {

    private final String inputJson = """
            {
              "id": 123,
              "name": "Bob Loblaw",
              "lastLogin": "2020-02-20T20:20:20Z",
              "roles": [
                {
                  "id": 1,
                  "name": "Manager"
                }
              ]
            }""";

    private final User user = new User(
        123,
        "Bob Loblaw",
        Instant.parse("2020-02-20T20:20:20Z"),
        singletonList(new Role(1, "Manager"))
    );

    @BeforeClass
    public static void setUp() {
        JDux.setPretty();
    }

    @Test
    public void parseAndOutputUserRecord() {
        var node = JDux.parse(inputJson);
        var user = node.asA(User.class);
        assertEquals(this.user, user);
        assertEquals(inputJson, JDux.node(user).jsonString());
    }

    @Test
    public void serializationOfUser() {
        var node = JDux.parse(inputJson);
        var unpretty = "{" +
            "\"id\":123,\"name\":\"Bob Loblaw\"," +
            "\"lastLogin\":\"2020-02-20T20:20:20Z\"," +
            "\"roles\":[{\"id\":1,\"name\":\"Manager\"}]" +
        "}";
        assertEquals(unpretty, node.toString());
    }

    @Test
    public void prettyOutput() {
        var node = JDux.parse(inputJson);
        assertEquals(inputJson, node.jsonString());
    }

    @Test
    public void reflection() {
        var node = JDux.node(user);
        assertEquals(inputJson, node.jsonString());
    }

}
