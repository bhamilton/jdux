package com.gitlab.bhamilton.jdux;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class StreamingFileDBTest extends AbstractJsonDBTest {

    @Override
    protected JsonDB getDB(String key) throws IOException {
        Path temp = Files.createTempFile(key, "json");
        return JDux.fileDB(temp);
    }

}
