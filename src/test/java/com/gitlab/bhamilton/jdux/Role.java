package com.gitlab.bhamilton.jdux;

public record Role(int id, String name) {}
