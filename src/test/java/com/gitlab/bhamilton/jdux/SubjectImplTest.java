package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.util.Subject;
import org.junit.Test;

import static org.junit.Assert.*;

public class SubjectImplTest {

    @Test
    public void test() {
        Subject<String> subject = Subject.of("test");
        assertEquals("test", subject.get());
        StringBuilder sb1 = new StringBuilder(), sb2 = new StringBuilder();
        subject.sync(sb1::append);
        for (int i=0; i < 10; i++)
            subject.update(i + "");
        subject.subscribe(sb2::append);
        for (int i=0; i < 10; i++)
            subject.update(i + "");
        assertEquals("test01234567890123456789", sb1.toString());
        assertEquals("0123456789", sb2.toString());
    }

}
