package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.db.IORuntimeException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class TestUtils {

    // the output json can be longer than the input in some cases
    public static final int SCALE_FACTOR = 2;

    public static JsonDB dbFromResource(String resource) {
        try {
            String json = toString(TestUtils.class.getResourceAsStream(resource));
            return JDux.memDB(json.length() * SCALE_FACTOR).root(JDux.parse(json));
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    public static String toString(InputStream in) throws IOException {
        byte[] buffer = new byte[512];
        ByteArrayOutputStream out = new ByteArrayOutputStream(in.available());
        int read;
        while ((read = in.read(buffer)) > 0)
            out.write(buffer, 0, read);
        return out.toString();
    }

}
