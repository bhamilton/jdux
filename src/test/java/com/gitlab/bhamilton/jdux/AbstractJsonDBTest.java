package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.db.JsonWriter;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.gitlab.bhamilton.jdux.JDux.parse;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public abstract class AbstractJsonDBTest {

    static final String SIMPLE_JSON = """
        {
          "user": {
            "name": "Steve",
            "age": 34
          },
          "friends": [
            {
              "name": "Joe",
              "age": 41
            },
            {
              "name": "Eddie",
              "age": 36
            }
          ]
        }""";

    static final String REFERENCE_JSON = """
        {
          "users": [
            {
              "name": "Steve",
              "age": 34,
              "friends": ["#users?name='Joe'", "#users?name='Eddie'"]
            },
            {
              "name": "Joe",
              "age": 41
              "friends": ["#users?name='Steve'"]
            },
            {
              "name": "Eddie",
              "age": 36,
              "friends": ["#users?name='Steve'"]
            }
          ]
        }""";


    private static final String PERSON_SCHEMA = """
        {
          "type": "object",
          "properties": {
            "name": {
              "type": "string"
            },
            "age": {
              "type": "number"
            }
          }
        }
        """;

    private static final String SIMPLE_JSON_SCHEMA = String.format("""
        {
          "type": "object",
          "properties": {
            "user": %s,
            "friends": {
              "type": "array",
              "items": %s
            }
          }
        }
        """, PERSON_SCHEMA, PERSON_SCHEMA);

    private final JsonWriter writer = new JsonWriter().setPretty(true);

    protected JsonDB db, refDb;
    protected List<JsonNode> updates;

    @Before
    public void setUp() throws Exception {
        db = getDB("simple").root(parse(SIMPLE_JSON));
        refDb = getDB("references").root(parse(REFERENCE_JSON));
        updates = new ArrayList<>();
    }

    protected abstract JsonDB getDB(String key) throws Exception;

    @Test
    public void addFriend() {
        TestUserRecord bob = new TestUserRecord("Bob", 43);
        db.subscribe("friends", updates::add);
        db.insert("friends", bob);
        db.insert("friends", asList(new TestUserRecord("John", 23), new TestUserRecord("Dimitri", 55)));
        String expected = """
            {
              "user": {
                "name": "Steve",
                "age": 34
              },
              "friends": [
                {
                  "name": "Joe",
                  "age": 41
                },
                {
                  "name": "Eddie",
                  "age": 36
                },
                {
                  "name": "Bob",
                  "age": 43
                },
                {
                  "name": "John",
                  "age": 23
                },
                {
                  "name": "Dimitri",
                  "age": 55
                }
              ]
            }""";
        assertEquals(expected, writer.toString(db.root()));
        assertEquals(2, updates.size());
    //    assertEquals(bob, updates.get(0).children(TestUserRecord.class).findFirst().orElseThrow(AssertionError::new));
    }

    @Test
    public void changeUser() {
        TestUserRecord bob = new TestUserRecord("Bob Loblaw", 43);
        db.subscribe("user", updates::add);
        db.update("user", bob);
        db.insert("user", parse("{\"occupation\":\"attorney\"}"));
        String expected = """
            {
              "user": {
                "name": "Bob Loblaw",
                "age": 43,
                "occupation": "attorney"
              },
              "friends": [
                {
                  "name": "Joe",
                  "age": 41
                },
                {
                  "name": "Eddie",
                  "age": 36
                }
              ]
            }""";
        assertEquals(expected, writer.toString(db.root()));
        assertEquals(2, updates.size());
        assertEquals(bob, updates.get(0).asA(TestUserRecord.class));
    }

    @Test
    public void changeAllNames() {
        db.subscribe("user.name", updates::add);
        db.subscribe("friends..name", updates::add);
        db.update("..name", "Spartacus");
        String expected = """
            {
              "user": {
                "name": "Spartacus",
                "age": 34
              },
              "friends": [
                {
                  "name": "Spartacus",
                  "age": 41
                },
                {
                  "name": "Spartacus",
                  "age": 36
                }
              ]
            }""";
        assertEquals(expected, writer.toString(db.root()));
        assertEquals(3, updates.size());
    }

    @Test
    public void changeSpecificName() {
        db.subscribe("..name", updates::add);
        db.update("user.name", "Spartacus");
        String expected = """
            {
              "user": {
                "name": "Spartacus",
                "age": 34
              },
              "friends": [
                {
                  "name": "Joe",
                  "age": 41
                },
                {
                  "name": "Eddie",
                  "age": 36
                }
              ]
            }""";
        assertEquals(expected, writer.toString(db.root()));
        assertEquals(1, updates.size());
    }

    @Test
    public void select() {
        assertEquals("\"Steve\",\"Joe\",\"Eddie\"",
            db.select("..name").map(JsonNode::jsonString)
                .collect(Collectors.joining(",")));
    }

    @Test
    public void delete() {
        db.delete("friends");
        String expected = """
            {
              "user": {
                "name": "Steve",
                "age": 34
              }
            }""";
        assertEquals(expected, writer.toString(db.root()));
    }

    @Test
    public void updateWithSchema() {
        db.schema(parse(SIMPLE_JSON_SCHEMA).asA(JsonSchema.class));
        addFriend();

        // should fail
        try {
            db.insert("friends", JDux.parse("\"Not a person.\""));
        } catch (JsonSchemaException schemaException) {
            return;
        }
        fail("Should throw exception with invalid node update.");
    }

    @Test
    public void recursiveReferenceExpansion() {
        JsonWriter writer = new JsonWriter(true, "  ", 2);
        String json = writer.toString(refDb.select("users"));
        assertEquals("""
                [
                  [
                    {
                      "name": "Steve",
                      "age": 34,
                      "friends": [
                        {
                          "name": "Joe",
                          "age": 41,
                          "friends": [
                            {
                              "name": "Steve",
                              "age": 34,
                              "friends": [
                                "#users?name='Joe'",
                                "#users?name='Eddie'"
                              ]
                            }
                          ]
                        },
                        {
                          "name": "Eddie",
                          "age": 36,
                          "friends": [
                            {
                              "name": "Steve",
                              "age": 34,
                              "friends": [
                                "#users?name='Joe'",
                                "#users?name='Eddie'"
                              ]
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "name": "Joe",
                      "age": 41,
                      "friends": [
                        {
                          "name": "Steve",
                          "age": 34,
                          "friends": [
                            {
                              "name": "Joe",
                              "age": 41,
                              "friends": [
                                "#users?name='Steve'"
                              ]
                            },
                            {
                              "name": "Eddie",
                              "age": 36,
                              "friends": [
                                "#users?name='Steve'"
                              ]
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "name": "Eddie",
                      "age": 36,
                      "friends": [
                        {
                          "name": "Steve",
                          "age": 34,
                          "friends": [
                            {
                              "name": "Joe",
                              "age": 41,
                              "friends": [
                                "#users?name='Steve'"
                              ]
                            },
                            {
                              "name": "Eddie",
                              "age": 36,
                              "friends": [
                                "#users?name='Steve'"
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                ]""", json);
    }

    public record TestUserRecord(String name, int age) {}

}
