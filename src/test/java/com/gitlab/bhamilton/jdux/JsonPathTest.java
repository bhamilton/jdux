package com.gitlab.bhamilton.jdux;

import com.gitlab.bhamilton.jdux.db.JsonPath;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JsonPathTest {

    @Test
    public void parseOne() {
        JsonSelector selector = JsonPath.parse("foo");
        assertEquals(1, selector.length());
        String key = "foo";
        JsonDepth type = JsonDepth.CHILD;
        assertElemMatches(selector, type, key);
    }

    @Test
    public void parseChain() {
        JsonSelector selector = JsonPath.parse("foo.bar..doo.gar");
        assertEquals(4, selector.length());
        assertEquals(".foo.bar..doo.gar", selector.toString());
        assertElemMatches(selector, JsonDepth.CHILD, "foo");
        assertElemMatches(selector = selector.next(), JsonDepth.CHILD, "bar");
        assertElemMatches(selector = selector.next(), JsonDepth.DESCENDANT, "doo");
        assertElemMatches(selector.next(), JsonDepth.CHILD, "gar");
    }

    @Test
    public void parseConditions() {
        JsonSelector selector = JsonPath.parse("test..foo[bar > 1 & (name = 'Steve' | name = 'Joe')]");
        assertEquals(3, selector.length());
        assertEquals("bar > 1 & name = \"Steve\" | name = \"Joe\"", selector.tail().stringValue());
    }

    @Test
    public void queryStringCondition() {
        JsonSelector selector = JsonPath.parse("test..foo?bar=1&name='Steve'");
        assertEquals(3, selector.length());
        assertEquals("bar = 1 & name = \"Steve\"", selector.tail().stringValue());
    }

    private void assertElemMatches(JsonSelector elem, JsonDepth type, String key) {
        assertEquals(key, elem.key().get());
        assertEquals(type, elem.depth());
    }

}
