JDux
====

Easy JSON persistence / state management for modern (14+) Java.

Features include:
- parsing
- querying
- reflection
- writing
- pub / sub

Inspired by Redux, we provide a basic means for managing state
via transactions over a JSON model.  The model can be either in-memory
or persisted over files. 

## Usage

Familiarize yourself with the syntax through examples provided below:

```java
import static com.gitlab.bhamilton.jdux.JDux.*;

public class JDuxExamples {

    record User(int id, String name, int age){}

    String jsonString = """
        {
            "id": 123,
            "name": "Steve",
            "age": 37
        }
        """;

    public static void main(String[] args) {

        // Basic parsing / writing:
        User user = parse(jsonString).asA(User.class);
        String backToJson = writeToString(user);

        // Using a folderDB:
        //    It is treated as a JSON object where each property is a separate file
        JsonDB db = folderDB(Paths.get("db"));
        db.root("{'users':[]}");
        db.insert("users", user);
        db.select("users").map(asA(User.class))
            .forEach(user -> System.out.println(user.name()));
    }
}
```

### Querying

Queries loosely follows a more lenient form of the 
[JsonPath syntax](https://goessner.net/articles/JsonPath/).  You can use querying
when interacting with a `JsonDB` when updating, subscribing, or selecting.
