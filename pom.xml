<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.gitlab.bhamilton</groupId>
    <artifactId>jdux</artifactId>
    <version>1.1.5-SNAPSHOT</version>

    <name>JDux</name>
    <description>JSON mapping and state management library for simple Java applications.</description>
    <url>https://gitlab.com/bhamilton/jdux</url>

    <licenses>
        <license>
            <name>The Apache License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <name>Bruce Hamilton</name>
            <email>bruce@bardly.io</email>
            <organization>com.gitlab.bhamilton</organization>
            <organizationUrl>https://gitlab.com/bhamilton</organizationUrl>
        </developer>
    </developers>

    <scm>
        <connection>scm:git:git://gitlab.com/bhamilton/jdux.git</connection>
        <developerConnection>scm:git:git@gitlab.com:bhamilton/jdux.git</developerConnection>
        <url>https://gitlab.com/bhamilton/jdux</url>
        <tag>HEAD</tag>
    </scm>

    <properties>
        <maven.compiler.source>15</maven.compiler.source>
        <maven.compiler.trget>15</maven.compiler.trget>
        <maven.javadoc.skip>true</maven.javadoc.skip>
        <maven.javadoc.failOnError>false</maven.javadoc.failOnError>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.junit.vintage</groupId>
            <artifactId>junit-vintage-engine</artifactId>
            <version>5.7.0-M1</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <release>${maven.compiler.source}</release>
                    <compilerArgs>
                        <arg>--enable-preview</arg>
                    </compilerArgs>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M4</version>
                <configuration>
                    <argLine>--enable-preview</argLine>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                        <version>3.2.1</version>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar-no-fork</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <version>3.2.0</version>
                        <configuration>
                            <source>${maven.compiler.source}</source>
                            <detectJavaApiLink>false</detectJavaApiLink>
                            <additionalOptions>--enable-preview</additionalOptions>
                        </configuration>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>1.6</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-release-plugin</artifactId>
                        <version>3.0.0-M1</version>
                        <configuration>
                            <localCheckout>true</localCheckout>
                            <pushChanges>false</pushChanges>
                            <mavenExecutorId>forked-path</mavenExecutorId>
                            <arguments>-Dgpg.passphrase=${gpg.passphrase}</arguments>
                        </configuration>
                        <dependencies>
                            <dependency>
                                <groupId>org.apache.maven.scm</groupId>
                                <artifactId>maven-scm-provider-gitexe</artifactId>
                                <version>1.9.5</version>
                            </dependency>
                        </dependencies>
                    </plugin>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>1.6.7</version>
                        <extensions>true</extensions>
                        <configuration>
                            <serverId>ossrh</serverId>
                            <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                            <autoReleaseAfterClose>true</autoReleaseAfterClose>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>doclint-java8-disable</id>
            <activation>
                <jdk>[1.8,)</jdk>
            </activation>
            <properties>
                <javadoc.opts>-Xdoclint:none</javadoc.opts>
            </properties>
        </profile>
    </profiles>

</project>
